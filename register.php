<?php
add_shortcode('feddit_register', 'registration_form');
add_action('init', 'custom_registration_function');
 
// The callback function that will replace [book]
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}
function registration_form(){
	if (get_option("users_can_register", "1")) {
		if (is_user_logged_in()) {
			return "<p>You are already registered and logged in.</p>";
		} else {
	return	'<form class="register_page" action="' . $_SERVER['REQUEST_URI'] . '" method="post">
			<label for="username">Username <strong>*</strong></label><br>
			<input type="text" name="username" value="' . ( isset( $_POST['username'] ) ? $username : null ) . '">
			<br>
			<label for="password">Password <strong>*</strong></label><br>
			<input type="password" name="password" value="' . ( isset( $_POST['password'] ) ? $password : null ) . '">
			<br>
			<input type="submit" class="fat-button" name="submit" value="Register"/>
			</form>';
		}
	} else {
		return "<p>Registration is closed.</p>";
	}
}
function registration_validation($username, $password)  {
	global $reg_errors;
	$reg_errors = new WP_Error;
	if ( empty( $username ) || empty( $password ) ) {
		$reg_errors->add('field', 'Required form field is missing');
	}
	if ( 4 > strlen( $username ) ) {
		$reg_errors->add( 'username_length', 'Username too short. At least 4 characters is required' );
	}
	if ( username_exists($username) || $username == 'modmail' ){
		$reg_errors->add('user_name', 'Sorry, that username already exists!');
	}
	if ( ! validate_username( $username ) ) {
		$reg_errors->add( 'username_invalid', 'Sorry, the username you entered is not valid' );
	}
	if ( 5 > strlen( $password ) ) {
		$reg_errors->add( 'password', 'Password length must be greater than 5' );
	}
	if ( is_wp_error( $reg_errors ) ) {
		
	}
}
function complete_registration() {
    global $reg_errors, $username, $password;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        $userdata = array(
        'user_login'    =>   $username,
        'user_pass'     =>   $password,
        );
        $user = wp_insert_user( $userdata );
		if ( ! is_wp_error($user) ) {
			$user_login = wp_signon(array('user_login' => $username, 'user_password' => $password, 'remember' => true), false);
			if ( is_wp_error($user_login) ){
				echo $user_login->get_error_message();
			} else {
				wp_redirect(site_url()."?r=registered");
				exit;
			}
		}
    }
}
function custom_registration_function() {
    if ( isset($_POST['submit'] ) ) {
		if($_POST['submit'] == "Register"){
			registration_validation(
			$_POST['username'],
			$_POST['password']
			);
			 
			// sanitize user form input
			global $username, $password;
			$username   =   sanitize_user( $_POST['username'] );
			$password   =   esc_attr( $_POST['password'] );
	 
			// call @function complete_registration to create the user
			// only when no WP_error is found
			complete_registration(
			$username,
			$password
			);
		}
    }
}