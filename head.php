<?php

// This page is for all the Login/Logout/Register finctions, plus the submit post button as well

function feddit_head(){
	global $wpdb;
	echo '<div id="f_head_links">'.PHP_EOL;
	if(is_user_logged_in()){
		$user = wp_get_current_user();
		$allow = get_option("feddit_allow_submit", "on");
		echo ($allow) ? '<div class="fat-button unselect" id="fat_submit">Submit post</div>'.PHP_EOL : '';
		echo '<div class="fat-button unselect" id="fat_logout"><a href="'.wp_logout_url().'">Logout</a></div>'.PHP_EOL;
		echo '<div class="fat-button unselect" id="fat_settings"><a href="'.site_url().'/u/'.$user->user_nicename.'/?tab=settings"><img src="'.plugins_url().'/feddit/images/settings.png" /></a></div>'.PHP_EOL;
		if (get_option("feddit_enable_msg", "")) {
			$input = $wpdb->get_var('SELECT COUNT(*) FROM '.$wpdb->prefix . 'msg WHERE `to` = '.$user->ID.' AND `read` = 0 AND `type` NOT LIKE \'n\';');
			echo '	<div class="fat-button unselect" id="fat_message"><a href="'.site_url().'/m/"><img src="'.plugins_url().'/feddit/images/message-'.(($input||get_admin_new($user->ID))&&!preg_match('/\/m\//',$_SERVER['REQUEST_URI'])?'new':'none').'.png" /></a></div>'.PHP_EOL; 
		}
		if(current_user_can('delete_others_feddits')){
			$input = $wpdb->get_var('SELECT COUNT(*) FROM '.$wpdb->prefix . 'msg WHERE `type` = \'m\' AND `read` = 0;');
			echo '	<div class="fat-button unselect" id="fat_modmail"><a href="'.site_url().'/wp-admin/admin.php?page=feddit&tab=feddit_modmail"><img src="'.plugins_url().'/feddit/images/cake-'.(($input)?'new':'white').'.png" /></a></div>'.PHP_EOL; 
		}
		if($allow){
			echo '<div id="fph_submit" class="fph_poppy">'.PHP_EOL;
				echo '<h3>Submit post</h3>'.PHP_EOL;
				echo '<form action="'. esc_url( admin_url('admin-post.php') ).'" method="post">';
					echo '<input type="hidden" name="action" value="fph_submit_form">';
					echo '<input type="hidden" name="return_url" value="'.'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'">';
					echo '<p>Title:<br><input type="text" name="submit_title" id="submit_title" class="input" /></p>
						  <p>URL: <sub><em>(Optional)</em></sub><br><input type="text" name="submit_url" id="submit_url" class="input"  /></p>
						  <p>Text: <sub><em>(Optional)</em></sub><br><textarea name="submit_text"></textarea></p>';
					echo '<div id="labels_label">Labels</div>';
					echo '<input type="submit" class="fat-button" value="Submit" id="submit" />';
					echo '<div id="labels_list">';
					$labels = get_terms("label", array('hide_empty' => false));
					$GLOBALS["labels"] = $labels;
					foreach ($labels as $l) {
						echo '<input type="checkbox" name="labels[]" id="label'.$l->term_id.'" value="'.$l->name.'" autocomplete="off">
						<label title="'.$l->description.'" for="label'.$l->term_id.'"><span>'.$l->name.'</span></label>';
					}
					echo '</div>';
				echo '</form>';
			echo '</div>'.PHP_EOL;
		}
	} else {
		echo '<div class="fat-button unselect" id="fat_login">Login</div>'.PHP_EOL;
		if(get_option("users_can_register", "1")){
			echo '<div class="fat-button unselect" id="fat_register">Register</div>'.PHP_EOL;
		}
		echo '<div id="fph_login" class="fph_poppy">'.PHP_EOL;
		echo '<h3>Login</h3>'.PHP_EOL;
		wp_login_form(array('label_username' => __( 'Username:' ),'label_password' => __( 'Password:' ),'value_remember' => true));
		echo '</div>'.PHP_EOL;
		echo '<div id="fph_register" class="fph_poppy">'.PHP_EOL;
		echo '<h3>Register</h3>'.PHP_EOL;
		echo do_shortcode('[feddit_register]');
		echo '</div>'.PHP_EOL;
	}
	if($_GET["r"]){
		$r["queued"]["head"] = "Post awaiting approval";
		$r["queued"]["body"] = "Your post has been successfully submitted. Wait for an admin to approve it before it gets displayed to the public.";
		$r["published"]["head"] = "Post published";
		$r["published"]["body"] = "Your post has been published for all to see.";
		$r["badurl"]["head"] = "Bad URL";
		$r["badurl"]["body"] = "The URL you submitted is invalid. Please type in a full URL or contact an admin.";
		$r["registered"]["head"] = "Registration successful";
		$r["registered"]["body"] = "You can now participate in the community.";
		$r["pass_set"]["head"] = "Password reset";
		$r["pass_set"]["body"] = "Your password has been updated. Don't lose it!";
		$r["bad_pass"]["head"] = "Wrong password";
		$r["bad_pass"]["body"] = "You entered the wrong password. Please try again.";
		$r["bad_pm"]["head"] = "User not found";
		$r["bad_pm"]["body"] = "The username you entered was not found. Please try again.";
		
		echo '<div id="fph_alert" class="fph_poppy" style="display:block;">'.PHP_EOL;
		echo '<h3>'.$r[$_GET["r"]]["head"].'<img src="'.plugins_url()."/feddit/images/x.png".'"></h3>'.PHP_EOL;
		echo '<p> '.$r[$_GET["r"]]["body"].'</p>';
		echo '</div>'.PHP_EOL;
	}
	echo '</div><br>'.PHP_EOL;
}
add_action("wp_ajax_fphlogout", "fphlogout");
add_action("wp_ajax_nopriv_fphlogout", "fphlogout");
function fphlogout() {
	wp_logout();
}
// Add CSS to login page
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/fatpeoplehate.png);
			background-color:#0005;
            padding-bottom: 30px;
			width:100%;
			max-width:700px;
			background-size:95%;
			background-position:center;
			border-radius: 5px;
        }
		body.login {
			background: 
			-webkit-repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			-webkit-repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			-webkit-repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			-webkit-repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			-webkit-repeating-linear-gradient(45deg, rgba(255, 255, 255, .5), rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			-webkit-repeating-linear-gradient(-45deg, rgba(255, 255, 255, .5) , rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			-webkit-repeating-linear-gradient(45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px), 
			-webkit-repeating-linear-gradient(-45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px);
			
			  background: 
			-moz-repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			-moz-repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			-moz-repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			-moz-repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			-moz-repeating-linear-gradient(45deg, rgba(255, 255, 255, .5), rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			-moz-repeating-linear-gradient(-45deg, rgba(255, 255, 255, .5) , rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			-moz-repeating-linear-gradient(45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px), 
			-moz-repeating-linear-gradient(-45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px);
			
			  background: 
			-o-repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			-o-repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			-o-repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			-o-repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			-o-repeating-linear-gradient(45deg, rgba(255, 255, 255, .5), rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			-o-repeating-linear-gradient(-45deg, rgba(255, 255, 255, .5) , rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			-o-repeating-linear-gradient(45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px), 
			-o-repeating-linear-gradient(-45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px);
			
			  background: 
			repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 90px), 
			repeating-linear-gradient(-45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			repeating-linear-gradient(45deg, rgba(24, 24, 24, .1) 60px, rgba(24, 24, 24, .9) 70px, transparent 1px, transparent 180px), 
			repeating-linear-gradient(45deg, rgba(255, 255, 255, .5), rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			
			repeating-linear-gradient(-45deg, rgba(255, 255, 255, .5) , rgba(255, 255, 255, .5) 1px, transparent 1px, transparent 180px), 
			repeating-linear-gradient(45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px), 
			repeating-linear-gradient(-45deg, rgba(255, 255, 255, .2) , rgba(255, 255, 255, .2) 1px, transparent 1px, transparent 90px);
			
			background-color: #181818;
			background-size: 150%, 256px 256px, 256px 256px, 256px 256px, 256px 256px, 256px 256px, 256px 256px, 256px 256px, 256px 256px;
			width: 100%
			margin: 0px;
			padding-top:30px;
		}
		#loginform, #registerform {
			border-radius: 5px;
			border:1px solid #000;
			background-color:#eee;
			padding:20px;
		}
		#nav, #backtoblog, #nav a, #backtoblog a {
			color:#eee !important;
			text-shadow: 0px 0px 5px #000;
		}
    </style>
	<script>
		
	</script>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
// Change the header image attributes without relying on JS
function login_title() {
    return site_url();
}
function login_words() {
    return bloginfo('name');
}
add_filter( 'login_headerurl', 'login_title' );
add_filter( 'login_headertitle', 'login_words' );
// Change registration message
function change_register_page_msg($message)
{
	if(strpos($message,"Register For This Site") == true) {
		$message = '<p class="message">Register for '.bloginfo('name').'!</p>';
	}
	return $message;
}
add_filter('login_message','change_register_page_msg');
?>