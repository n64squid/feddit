<?php
add_filter('get_comments_number', 'fph_comment_count', 0);
function fph_comment_count($count) {
        if ( ! is_admin() ) {
                global $id;
                $comments_tmp = get_comments('status=approve&post_id=' . $id);
                $comments_sep = separate_comments($comments_tmp);
                $comments_by_type = &$comments_sep;
                return count($comments_by_type['comment']);
        } else {
                return $count;
        }
}

function fph_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	$args = get_comment_meta($comment->comment_ID,"report_status");
	$can = current_user_can('delete_others_feddits');
	$class = ($args[0] == 'removed' && $can) ? 'removed' : '';
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'picolight' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'picolight' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class($class); ?> id="li-comment-<?php comment_ID(); ?>">
	<?php
	if(get_option("feddit_allow_vote", "on") == "on"){ 
		$nonce = wp_create_nonce("my_user_vote_nonce");
		$uid = get_current_user_id();
		echo '<div class="votediv">';
			echo '<img class="upvote vote" id="upvote'.$comment->comment_ID.'" vote="up" type="c" data-nonce="' . $nonce . '" data-post_id="' . $comment->comment_ID . '" src="'.get_vote_img(true,$comment->comment_ID,$uid,"c").'" /><br />';
			 echo get_post_votes($comment->comment_ID,"c")."<br />"; 
			 echo '<img class="upvote vote" id="downvote'.$comment->comment_ID.'" vote="down" type="c" data-nonce="' . $nonce . '" data-post_id="' . $comment->comment_ID . '" src="'.get_vote_img(false,$comment->comment_ID,$uid,"c").'" />';
		echo '</div>';
	} 
	?>
		<div id="comment-<?php comment_ID(); ?>" class="comment comment_content">
			<div class="comment-author vcard">
				<?php
					if ($args[0] != 'removed'){
						comment_author();
						echo " on ";
						comment_date();
						echo ", ";
						comment_time();
						if($can){
							edit_comment_link( __( '(Edit)', 'picolight' ), '<span class="edit-link">', '</span>' );
						}
					} else {
						echo "[removed]";
					}
				?>
			</div>
			
			<div class="comment-content">
				<?php
					if ($args[0] != 'removed'){
						comment_text(); 
					} else {
						echo "<p>[removed]</p>";
					}
				?>
			</div>

			<div class="reply">
				<?php 
				$reply = get_comment_reply_link( array_merge( $args, array( 'reply_text' => '[Reply]', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
				//echo "(".get_comment_ID().":".get_the_ID().")";
				//echo "(".$reply.")";
				echo feddit_post_buttons(
					array(
						"reply", 
						"report", 
						"save",
						"mod"
					),
					array(
						"id" => $comment->comment_ID,
						"t" => "c",
						"reply" => $reply,
						"state" => get_comment_meta($comment->comment_ID,"report_status")
					)
				);
				?>
			</div><!-- .reply -->
		</div><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
add_action('wp_insert_comment','comment_inserted');
function comment_inserted($comment_id) {
	if (!get_user_meta(get_current_user_id(),"ban")){
		// Send OK
	} else {
		wp_delete_comment($comment_id);
		die ('You\'ve been banned and cannot comment');
	}
}
?>