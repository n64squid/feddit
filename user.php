<?php
function add_query_vars($aVars) {
	$aVars[] = "u";
	return $aVars;
}
// hook add_query_vars function into query_vars
add_filter('query_vars', 'add_query_vars');
function add_rewrite_rules($aRules) {
$aNewRules = array('u/(.+)/?$' => 'index.php?pagename=u&u=$matches[1]');
$aRules = $aNewRules + $aRules;
return $aRules;
}
function user_heading_func($heading){
	global $post;
	global $wp_query;
	if($post->post_name != "u" || !in_the_loop()){
		// Do nothing
	} else if ($wp_query->query_vars["u"]) {
		if($user = get_user_by("slug",$wp_query->query_vars["u"])){
			$name = $user->data->user_login;
			$heading = $name."'s profile";
		} else {
			$heading = "User not found";
		}
	} else {
		$heading = "User list";
	}
	return $heading;
}
add_filter('the_title', 'user_heading_func');

function user_title_func($title){
	global $post;
	global $wp_query;
	if($post->post_name != "u"){
		// Do nothing
	} else  if ($wp_query->query_vars["u"]) {
		if($user = get_user_by("slug",$wp_query->query_vars["u"])){
			$name = $user->data->user_login;
			$title["title"] = $name."'s profile";
		} else {
			$title["title"] = "User not found";
		}
	} else {
		$title["title"] = "User list";
	}
	return $title;
}
add_filter('document_title_parts', 'user_title_func');

add_filter('rewrite_rules_array', 'add_rewrite_rules');
function user_page_func($textaroni){
	global $post;
	global $wp_query;
	$out = "";
    $post_slug=$post->post_name;
	if($post->post_name == "u"){
		if ($wp_query->query_vars["u"]) {
			if($user = get_user_by("slug",$wp_query->query_vars["u"])){
				$id = $user->data->ID;
				$curr = $id == get_current_user_id();
				$tab = isset($_GET['tab']) ? $_GET['tab'] : 'front';
				$s = ' selected';
				if($curr){
					$out .= "<div id=\"user_tabs\">";
						$out .= "<span tab=\"front\" class=\"".(($tab =='front')?$s:'')."\">Front</span>";
						$out .= "<span tab=\"settings\" class=\"".(($tab =='settings')?$s:'')."\">Settings</span>";
						$out .= "<span tab=\"saved\" class=\"".(($tab =='saved')?$s:'')."\">Saved</span>";
					$out .= "</div>";
				}
				$out .= "<div id=\"user_front\" class=\"user_wrap".(($tab =='front')?$s:'')."\">";
					$len = get_option("short_length", 250);
					$out .= "<div class=\"user_card user_head\">";
						$out .= "<div id=\"user_score\">";
							$out .= "<h2>User score</h2>";
							$vgp = get_user_votes($id, "p", false);
							$vgc = get_user_votes($id, "c", false);
							$out .= "Votes given: <span title=\"Post score\">".$vgp."</span>".($vgc<0?"":"+")."<span title=\"Comment score\">".$vgc."</span>=<span title=\"Total score\">".($vgp+$vgc)."</span>";
							$out .= "<br>";
							$vrp = get_user_votes($id, "p", true);
							$vrc = get_user_votes($id, "c", true);
							$out .= "Votes received: <span title=\"Post score\">".$vrp."</span>".($vrc<0?"":"+")."<span title=\"Comment score\">".$vrc."</span>=<span title=\"Total score\">".($vrp+$vrc)."</span>";
						$out .= "</div>";
						$out .= "<div id=\"user_trophies\">";
						$out .= "<h2>Trophies</h2> (Under development)";
						$out .= "</div>";
					$out .= "</div>";
					$out .= "<h2>User history</h2>";
					//$my_query = new WP_Query( array('author' => $id, 'post_type' => 'feddit') );
					$taxa = get_the_terms($id,"label") ?  get_the_terms($id,"label") : array(""); 
					$GLOBALS['taxa'] = $taxa;
					$usercomments = get_comments('user_id='.$id);
					$limit = get_option("feddit_number", 10);
					$userposts = get_posts(array('posts_per_page' => $limit, 'post_type' => 'feddit', 'author' => $id));
					$most_recent = array();
					foreach ($usercomments as $c) {
						$c->fecha = strtotime($c->comment_date_gmt);
						array_push($most_recent, $c);
					}
					foreach ($userposts as $p){
						$p->fecha = strtotime($p->post_date_gmt);
						array_push($most_recent, $p);
					}
					//ksort($most_recent);
					usort($most_recent, function($a, $b) {
						return $b->fecha - $a->fecha;
					});
					$most_recent = array_slice($most_recent, 0, 10);
					foreach ($most_recent as $m){
						if($m->ID){
							$out .= display_feddit($m->ID,"p",get_option("feddit_allow_vote_user", "off") == "on");
						} else {
							$out .= display_feddit($m->comment_ID,"c",get_option("feddit_allow_vote_user", "off") == "on");
						}
					}
				$out .= "</div>";
				if ($curr) {
					$out .= "<div id=\"user_settings\" class=\"user_wrap".(($tab =='settings')?$s:'')."\">";
						$out .= "<div class=\"user_head\">";
							$out .=	'<form class="" action="' . $_SERVER['REQUEST_URI'] . '" method="post">
									<label for="old_p">Current Password</label><br>
									<input type="password" name="old_p" value="">
									<br>
									<label for="new_p">New Password</label><br>
									<input type="password" name="new_p" value="">
									<br>
									<input class="fat-button" type="submit" name="submit" value="Change Password">
									</form>';
						$out .= "</div>";
						if ($f=get_the_author_meta("flair",$post->post_author)){
							if (isset ($GLOBALS['flair_alert'])){
								if ($GLOBALS['flair_alert']=='success'){
									$out .= "Your flair has been updated.";
								} else if ($GLOBALS['flair_alert']=='deleted') {
									$out .= "You have deleted your flair. Please contact an admin to get it back.";
								}
								
							}
							$out .= "<div class=\"user_head\">";
								$out .=	'<form class="" action="' . $_SERVER['REQUEST_URI'] . '" method="post">
										<label for="old_p">Change flair</label><br>
										<input type="text" name="flair" value="'.$f.'">
										<br>
										<input class="fat-button" type="submit" name="submit" value="Change Flair">
										</form>';
							$out .= "</div>";
							}
					$out .= "</div>";
					$out .= "<div id=\"user_saved\" class=\"user_wrap".(($tab =='saved')?$s:'')."\">";
						$o = false;
						if($arr = get_user_meta($id,'saved')){
							$ps = json_decode($arr[0],true);
							$o = true;
						} else {
							$ps = array();
						}
						$out .= "<div class=\"user_head\">";
							$out .= $o?"Below are your saved posts.":"You have no saved posts.";
						$out .= "</div>";
						$ps = array_reverse($ps, true);
						if ($o){
							foreach($ps as $p){
								$out .= display_feddit($p['p'],$p['t'],get_option("feddit_allow_vote_user", "off") == "on");
							}
						}
					$out .= "</div>";
				}
			} else {
				$out = "User not found. Did you click on the right link?";
			}
			return $out;
		} else {
			// User list
			$out = "";
			$user_query = new WP_User_Query( array(
				'number' => get_option("feddit_number", 10),
				'meta_key' => 'score',
				'meta_query' => array(
					'key'     => 'score'
				),
				'orderby' => 'meta_value',
				'order' => 'DESC'
				));
			foreach ($user_query->results as $u){
				$f=get_user_meta($u->data->ID,"flair");
				$f = ($f) ? "[".$f[0]."]" : "";
				$out.= '<p><a href="'.site_url()."/u/".$u->data->user_nicename.'/">'.$u->data->user_login."</a> ".$f."</p>";
			}
			
			return $out;
		}
	} else {
		return $textaroni;
	}
}
add_filter('the_content', 'user_page_func');