<?php
function feddit_submit() {
	
	if (!get_user_meta(get_current_user_id(),"ban")){
	
		$okiedoke = true; // Leave be unless something goes wrong
		
		// Get _POST data
		$title = $_POST["submit_title"];
		$text = $_POST["submit_text"];
		$url = $_POST["submit_url"];
		$vars = explode("?",$_POST["return_url"]);
		$returnurl = $vars[0];
		$labels = $_POST["labels"];
		
		// Modify Slug
		$urltitle = strtolower(preg_replace(array('/ +/','/[!$£%^&*()_+|~=`{}\[\]:";\'<>?,.]/','/\//','/--/'),array('-','','-','-'),$title));
		$slug = substr(str_shuffle(strtolower(preg_replace('/[- !$£%^&*()_+|~=`{}\[\]:";\'<>?,.\/]/','',$title))),0,7)."-".$urltitle;
		
		// URL Regex Jeffrey Friedl
		$regex="/(https?|ftp):\/\/(-\.)?([^\s\/?\.#-]+\.?)+(\/[^\s]*)?$/";
		// Check URL
		if (!preg_match($regex,$url) && $url!=""){
			$returnurl .= "?r=badurl";
		} else {
			if ($url==""){
				$url = site_url("/f/".$slug."/");
			}
			// Set post details
			$my_post = array(
				'post_type'    => "feddit",
				'post_title'    => htmlentities($title),
				'post_content'  => $text,
				'post_author'   => get_current_user_id(),
				'post_name' => $slug,
			);
			if (current_user_can('publish_feddit') || get_option("feddit_require_approval", "")) {
				$my_post["post_status"] = 'publish';
				$returnurl .= "?r=published";
			} else {
				$my_post["post_status"] = 'draft';
				$returnurl .= "?r=queued";
			}
			$id = wp_insert_post( $my_post );
			wp_set_object_terms( $id, $labels, 'label' );
			add_post_meta( $id, '_url', $url, true );
		}
		wp_redirect($returnurl);
	} else {
		die ('You have been banned and cannot post.');
	}
}
//add_action( 'admin_post_nopriv_fph_submit_form', 'feddit_submit' );
add_action( 'admin_post_fph_submit_form', 'feddit_submit' );


// Add permissions
add_action('admin_init','add_role_caps');
function add_role_caps() {
	$role = get_role('subscriber');
		$role-> add_cap('edit_feddit');
		$role-> add_cap('edit_feddits');
	$role = get_role('administrator');
		$role-> add_cap('read_feddit');
		$role-> add_cap('read_private_feddit');
		$role-> add_cap('edit_feddit');
		$role-> add_cap('edit_feddits');
		$role-> add_cap('publish_feddit');
		$role-> add_cap('edit_others_feddit');
		$role-> add_cap('delete_feddit');
		$role-> add_cap('delete_others_feddits');
		$role-> add_cap('edit_labels');
		$role-> add_cap('publish_labels');
		$role-> add_cap('read_label');
}

// Filters for content
add_filter( 'the_content', 'feddit_bbcodes');
add_filter( 'comment_text', 'feddit_bbcodes');
function feddit_bbcodes($content){
	
	// Strip HTML
	global $post;
	$type = get_post_type($post);
	if($type!='post' && $type!='page'){
		$content = strip_tags($content);
	}
	
	$content = preg_replace("/\*\*(.+)\*\*/","<strong>$1</strong>",$content);// Bold
	$content = preg_replace("/\*(.+)\*/","<em>$1</em>",$content);// Italics
	$content = preg_replace("/~(.+)~/","<strike>$1</strike>",$content);// Strikethrough
	$content = preg_replace("/\[(.+)\]\((.+)\)/","<a rel=\"nofollow\" href=\"$2\">$1</a>",$content);// Anchors
	
	return $content;
}
add_action( 'init', 'feddit_redirect' );
function feddit_redirect($content){
	if(isset($_GET['action']) && $_GET['action'] == 'register'){
		if (get_option("feddit_register_url", "") != ""){
			wp_redirect(site_url().get_option("feddit_register_url", ""));
			exit();
		}
	}
}

?>