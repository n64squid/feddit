<?php
/**
 * Plugin Name: Feddit
 * Plugin URI: https://n64squid.com
 * Description: This plugin is made to replicate Reddit functionality in wordpress
 * Version: 0.9
 * Author: Shmuk Lidooha
 * Author URI: https://n64squid.com
 * License: GPL2
 */
 
// function to create the DB / Options / Defaults					
function feddit_install() {
   	global $wpdb;
  	$vote_table = $wpdb->prefix . 'votes';
  	$msg_table = $wpdb->prefix . 'msg';
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	if($wpdb->get_var("show tables like '$msg_table'") != $msg_table) {
		$sql = "CREATE TABLE " . $msg_table . " (
		`id` mediumint(9) NOT NULL AUTO_INCREMENT,
		`from` mediumint(9) NOT NULL,
		`to` mediumint(9) NOT NULL,
		`msg` text NOT NULL,
		`read` tinyint(1) NOT NULL,
		`type` char(1) NOT NULL,
		`msg_date` int NOT NULL,
		UNIQUE KEY id (id)
		) ".$wpdb->get_charset_collate().";";
		dbDelta($sql);
	}
	set_feddit_settings();
}
register_activation_hook(__FILE__,'feddit_install');

// Create the custom feddit post type
function wpt_feddit_posttype() {
	register_post_type( 'feddit',
		array(
			'labels' => array(
				'name' => __( 'Feddit posts' ),
				'singular_name' => __( 'Feddit' ),
				'add_new' => __( 'Add New Feddit' ),
				'add_new_item' => __( 'Add New Feddit' ),
				'edit_item' => __( 'Edit Feddit' ),
				'new_item' => __( 'Add New Feddit' ),
				'view_item' => __( 'View Feddit' ),
				'search_items' => __( 'Search Feddit' ),
				'not_found' => __( 'No Feddits found' ),
				'not_found_in_trash' => __( 'No Feddits found in trash' )
			),
			'public' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
			'capability_type' => 'post',
			'rewrite' => array("slug" => "f"), // Permalinks format
			'menu_position' => 5,
			'menu_icon' => 'dashicons-fph-edit',
			'register_meta_box_cb' => 'add_feddit_url',
			'capabilities' => array(
				'publish_posts' => 'publish_feddit',
				'edit_posts' => 'edit_feddits',
				'edit_others_posts' => 'edit_others_feddit',
				'read_private_posts' => 'read_private_feddit',
				'edit_post' => 'edit_feddit',
				'delete_post' => 'delete_feddit',
				'delete_others_posts' => 'delete_others_feddits',
				'read_post' => 'read_feddit'
			),
			'taxonomies'  => array('label')
		)
	);
}
add_action( 'init', 'wpt_feddit_posttype' );

// Create a custom taxonomy for categorising feddits
function people_init() {
	register_taxonomy(
		'label',
		'feddit',
		array(
			'label' => __( 'Labels' ),
			'rewrite' => array( 'slug' => 'l' , 'with_front' => false),
			'capabilities' => array(
				'assign_terms' => 'edit_labels',
				'edit_terms' => 'publish_labels',
				'read_post' => 'read_label'
			)
		)
	);
}
add_action( 'init', 'people_init' );


// Register Custom Status for reported posts
function feddit_reported() {

	$args = array(
		'label'                     => _x( 'Reported', 'Status General Name', 'reported' ),
		'label_count'               => _n_noop( 'Reported (%s)',  'Reported (%s)', 'reported' ), 
		'public'                    => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => false,
	);
	register_post_status( 'reported', $args );

}
add_action( 'init', 'feddit_reported', 0 );



// Add the feddits Meta Boxes

function add_feddit_url() {
	add_meta_box('wpt_feddit_url', 'Feddit url', 'wpt_feddit_url', 'feddit', 'normal', 'high');
}

// The feddit url Metabox

function wpt_feddit_url() {
	global $post;
	
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="fedditmeta_noncename" id="fedditmeta_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	
	// Get the url data if its already been entered
	$url = get_post_meta($post->ID, '_url', true);
	
	// Echo out the field
	echo '<input type="text" name="_url" value="' . $url  . '" class="widefat" />';

}

// Save the Metabox Data

function wpt_save_feddit_meta($post_id, $post) {
	
	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['fedditmeta_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	$feddit_meta['_url'] = $_POST['_url'];
	
	// Add values of $feddit_meta as custom fields
	foreach ($feddit_meta as $key => $value) {// Cycle through the $feddit_meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if($value=="") delete_post_meta($post->ID, $key); // Delete if blank
	}
}
add_action('save_post', 'wpt_save_feddit_meta', 1, 2); // save the custom fields

function your_plugin_settings_link($links) { 
  $settings_link = '<a href="admin.php?page=feddit">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
 
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'your_plugin_settings_link' );

add_action( 'init', 'my_script_enqueuer' );
function my_script_enqueuer() {
   
   wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
   wp_enqueue_script( 'jquery' );
   wp_enqueue_script( 'my_voter_script' );

}


include ("admin/adminpanel.php");
include ("comments.php");
include ("css.php");
include ("head.php");
include ("loadposts.php");
include ("message.php");
include ("modlog.php");
include ("postfunc.php");
include ("register.php");
include ("submit.php");
include ("user.php");
include ("voting.php");