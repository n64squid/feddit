<?php
// Voting

add_action("wp_ajax_user_vote", "user_vote_func");
add_action("wp_ajax_nopriv_user_vote", "my_must_login");

function get_vote_img($dir,$pid,$uid,$typ) {
	global $wpdb;
	$folder = plugins_url()."/feddit/images/";
	$sql='SELECT * FROM '.$wpdb->prefix . 'votes'.' WHERE post = ' . $pid . " AND user = " . $uid . " AND type = '" . $typ . "'";
	$result = $wpdb->get_results( $sql , OBJECT );
	if(!is_null($result[0])){
		if($dir){
			if($result[0]->upvote == 1){
				return $folder."upvoted.png";
			} else {
				return $folder."upvote.png";
			}
		} else {
			if($result[0]->upvote == -1){
				return $folder."downvoted.png";
			} else {
				return $folder."downvote.png";
			}
		}
	} else {
		if($dir){
			return $folder."upvote.png";
		} else {
			return $folder."downvote.png";
		}
	}
}
function user_vote_func() {
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "my_user_vote_nonce")) {
		$result['type'] = "nonce";
		$result['nonce'] = "nonce";
		exit(json_encode($result));
	}  
	$direction = $_REQUEST['vote'];
	$vote_count = get_post_meta($_REQUEST["post_id"], "votes", true);
	$vote_count = ($vote_count == '') ? 0 : $vote_count;
	
	if ($direction != "down"){
		$upvote = 1;
		$result['dir'] = "up";
		$result['opp'] = "down";
	} else {
		$upvote = -1;
		$result['dir'] = "down";
		$result['opp'] = "up";
	}
	global $wpdb;
	$sql='SELECT * FROM '.$wpdb->prefix . 'votes'.' WHERE post = ' . $_REQUEST["post_id"] . " AND user = " . get_current_user_id();
	$count = $wpdb->get_results( $sql , OBJECT );
	if($wpdb->num_rows == 0){
		if($wpdb->insert($wpdb->prefix . 'votes', array( 'user' => get_current_user_id(), 'post' => $_REQUEST["post_id"], 'type' => $_REQUEST['type'], 'upvote' => $upvote, 'date' => time()))){
			$result['type'] = "vote";
		} else {
			$result['type'] = "sqlerror";
		}
	} else { // If user has already voted for this
		if ($count[0]->upvote == $upvote){
			$wpdb->delete($wpdb->prefix .'votes', array('user' => get_current_user_id(), 'post' => $_REQUEST["post_id"]));
			$result['type'] = "unvote";
		} else {
			$wpdb->update($wpdb->prefix .'votes', array('upvote' => $upvote), array('user' => get_current_user_id(), 'post' => $_REQUEST["post_id"]));
			$result['type'] = "switch";
		}
	}
	
	
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		$p = get_post($_REQUEST["post_id"]);
		
		$vrp = get_user_votes($p->post_author, "p", true);
		$vrc = get_user_votes($p->post_author, "c", true);
		update_user_meta($p->post_author,"score",($vrp+$vrc));
		echo $result;
	} else {
		header("Location: ".$_SERVER["HTTP_REFERER"]);
	}
	die();
}

function my_must_login() {
	$result['type'] = "login";
	$result = json_encode($result);
	echo $result;
	die();
}
function get_post_votes($post, $typ) {
	global $wpdb;
	$sql='SELECT SUM(upvote) AS votes FROM '.$wpdb->prefix . 'votes'.' WHERE post = ' . $post . " AND type = '" . $typ . "'";
	$results = $wpdb->get_results( $sql , OBJECT );
	if(!is_null($results[0]->votes)){
		return $results[0]->votes;
	} else {
		return 0;
	}
}
function get_user_votes($user, $typ = "p", $got = true) {
	global $wpdb;
	if ($got){
		if ($typ == "p"){
			$sql='	SELECT SUM(upvote) AS votes 
					FROM '.$wpdb->prefix.'votes'.' 
					LEFT JOIN '.$wpdb->prefix.'posts 
					ON '.$wpdb->prefix.'posts.ID = '.$wpdb->prefix.'votes.post
					WHERE '.$wpdb->prefix.'posts.post_author = ' . $user . ' 
					AND '.$wpdb->prefix.'votes.type = \'' . $typ . '\'';
		} else {
			$sql='	SELECT SUM(upvote) AS votes 
					FROM '.$wpdb->prefix.'votes'.' 
					LEFT JOIN '.$wpdb->prefix.'comments 
					ON '.$wpdb->prefix.'comments.comment_ID = '.$wpdb->prefix.'votes.post
					WHERE '.$wpdb->prefix.'comments.user_id = ' . $user . ' 
					AND '.$wpdb->prefix.'votes.type = \'' . $typ . '\'';
		}
		
	} else {
		$sql='SELECT COUNT(type) AS votes FROM '.$wpdb->prefix . 'votes'.' WHERE user = ' . $user . " AND type = '" . $typ . "'";
	}
	$results = $wpdb->get_results( $sql , OBJECT );
	//return $sql;
	if(!is_null($results[0]->votes)){
		return $results[0]->votes;
	} else {
		return 0;
	}
}
function feddit_sort_comments($comments) {
	foreach($comments as $c){
		$c->comvotes = (int)get_post_votes($c->comment_ID,"c");
	}
	function cmp($a, $b){
		return $b->comvotes - $a->comvotes;
	}
	usort($comments, "cmp");
	return($comments);
}	
add_filter ('comments_array', 'feddit_sort_comments');