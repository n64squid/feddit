<?php
// Voting

add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
  echo '<style>
    table {
   		border-collapse: collapse;
	}

	.users_feddit table, .users_feddit td {
		border: 1px solid black;
		padding:5px;
		max-width:100%;
	}
	.users_feddit td .flairinput, .loading {
		display:none;
	}
	.usertablehead {
		background-color: #d8d8d8;
		font-weight:bold;
	}
	.dashicons-fph {
		background-image: url("'.plugins_url().'/feddit/images/cake-gray.png");
		background-repeat: no-repeat;
		background-position: center; 
	}
	.dashicons-fph-edit {
		background-image: url("'.plugins_url().'/feddit/images/cake-pencil-gray.png");
		background-repeat: no-repeat;
		background-position: center; 
	}';
	if(is_admin()){
		echo '.mod_tools {
		display:block;
	}';
	}
	echo'
	</style>
	';
}
add_action('wp_head','fedditscripts');
add_action('admin_head','fedditscripts');
function fedditscripts() {
	echo '<script type="text/javascript">'.PHP_EOL;
	echo 'labels = {};'.PHP_EOL;
	$labels = get_terms("label", array('hide_empty' => false));
	foreach ($labels as $l) {
		echo 'labels["'.$l->slug.'"] = false;'.PHP_EOL;
	}
  	echo 'plugdir = "'.plugins_url().'";'.PHP_EOL;
	echo 'ajaxurl = "' . admin_url('admin-ajax.php') . '";'.PHP_EOL;
  	echo 'offset = '.get_option("feddit_number", 10).';'.PHP_EOL;
  	echo 'curr_offset = offset;'.PHP_EOL;
	if(isset($_SERVER["REQUEST_URI"]) && preg_match("/\?/",$_SERVER["REQUEST_URI"])){
		echo "history.replaceState( {} , '', '".preg_replace("/\?.+$/","",$_SERVER["REQUEST_URI"])."' );";
	}
	echo '</script>';
}
function feddit_css() {
    wp_enqueue_style( 'style-name', plugins_url()."/feddit/frontend.css" );
	wp_enqueue_script('admin_script',plugins_url( '/feddit/admin/settings.js'),array( 'jquery' ));
	wp_enqueue_script('frontend_script',plugins_url( '/feddit/script.js'),array( 'jquery' ));
    //wp_enqueue_style( 'jqueryui', "https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" );
	//wp_enqueue_script('script','https://code.jquery.com/ui/1.12.1/jquery-ui.js', array ( 'jquery' ), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'feddit_css' );

// Flair up the author
add_filter( 'the_author', 'feddit_author' );

function feddit_author( $name ) {
	global $post;
	$roles = get_the_author_meta("wp_capabilities", $post->post_author);
	$name='<a href="'.site_url()."/u/".get_the_author_meta("user_nicename",$post->post_author).'/">'.$name.'</a>';
	if($f = get_the_author_meta("flair",$post->post_author)){$name .= " <span class=\"flair ".key($roles)."\">[".$f."]</span>";}
	return $name;
}