<?php
add_shortcode('feddit_message', 'feddit_message_page');
function feddit_message_page(){
	$out = "";
	if (get_option("feddit_enable_msg", "")) {
		if(is_user_logged_in()){
		global $wpdb;
		$id = get_current_user_id();
		$tab = isset($_GET['tab']) ? $_GET['tab'] : 'inbox';
		$s = ' selected';
		$out .= "<div id=\"user_tabs\">";
			$out .= "<span tab=\"inbox\" class=\"".(($tab =='inbox')?$s:'')."\">Inbox</span>";
			$out .= "<span tab=\"sent\" class=\"".(($tab =='sent')?$s:'')."\">Sent</span>";
			$out .= "<span tab=\"compose\" class=\"".(($tab =='compose')?$s:'')."\">Compose</span>";
		$out .= "</div>";
		
		$out .= "<div id=\"user_inbox\" class=\"user_wrap".(($tab =='inbox')?$s:'')."\">";
			$sql='SELECT * FROM '.$wpdb->prefix . 'msg WHERE `to` = '.$id.' OR `type` = \'a\' ORDER BY msg_date DESC;';
			$input = $wpdb->get_results( $sql , OBJECT );
			$out .= display_pm($input,'in');
			$wpdb->update($wpdb->prefix .'msg', array('read' => 1), array('to' => get_current_user_id()));
		$out .= "</div>";
		
		$out .= "<div id=\"user_sent\" class=\"user_wrap".(($tab =='sent')?$s:'')."\">";
			$sql='SELECT * FROM '.$wpdb->prefix . 'msg WHERE `from` = '.$id.' ORDER BY msg_date DESC;';
			$input = $wpdb->get_results( $sql , OBJECT );
			$out .= display_pm($input,'out');
		$out .= "</div>";
		
		$out .= "<div id=\"user_compose\" class=\"user_wrap".(($tab =='compose')?$s:'')."\">";
			$out .= "<div class=\"user_head\">";
				$out .=	'<form class="" action="' . $_SERVER['REQUEST_URI'] . '" method="post">
						<label for="to">To:</label><br>
						<input type="text" name="to" id="compose_to" value=""> <a id="modmail">Modmail</a>
						<br>
						<label for="msg">Message</label><br>
						<textarea name="msg" id="compose_msg"></textarea>
						<br>
						<input class="fat-button" type="submit" name="submit" value="Send Message">
						</form>';
			//$out .= "<pre>".var_export(get_defined_vars(),true)."</pre>";
			$out .= "</div>";
		$out .= "</div>";
		
		} else {
			$out .= "<div class=\"user_card user_head\">You must be logged in to view this page</div>";
		}
	} else {
		$out .= "<div class=\"user_card user_head\">Messages are disabled.</div>";
	}
	return $out;
}
function feddit_send_PM($args) {
	if (!get_user_meta(get_current_user_id(),"ban") || $args['to']==0){
		$from = ($args['from']) ? $args['from'] : get_current_user_id();
		global $wpdb;
		if($wpdb->insert($wpdb->prefix . 'msg', array( 'from' => $from, 'to' => $args['to'], 'msg' => encrypt_decrypt('encrypt',  $args['msg'],get_salt($args['to'],$from)), 'type' => $args['type'], 'read' => 0, 'msg_date' => time()))){
		} else {
			if(current_user_can('administrator')){
				echo "<pre>";
				exit( var_dump( $wpdb->last_query ) );
				echo "<pre>";
			}
		}
	} else {
		die ('You are banned and can only send PMs to modmail to appeal.');
	}
}
function display_pm ($input, $type) {
	$out="";
	foreach ($input as $i) {
		$n=false;
		if ($i->type == 'a'){
			$u=get_current_user_id();
			if (get_admin_new($u)){
				$read = get_option('feddit_admin_read','');
				update_option('feddit_admin_read',$read.','.$u);
				$n = true;
			}
		}
		$out .= "<div class=\"feddit_inbox feddit_class_".$i->type.(($i->read||!$n)?" pm_read":"")."\">";
			$f = ($type == 'in') ? get_userdata($i->from) : get_userdata($i->to);
			$out .= '<div class="message_head">'.feddit_return_author($f->ID).' <span title="'.preg_replace("/ /",", ",date("Y-m-d G:i:s",$i->msg_date)).' GMT">'.time_elapsed_string(date("Y-m-d G:i:s",$i->msg_date)).'</span></div>';
			$out .= '<div class="message_msg">'.(($i->type=='p' || $i->type=='a' || $i->type=='m' || $i->type=='l') ? encrypt_decrypt('decrypt',$i->msg,get_salt($i->to,$i->from)) : get_comment_text(encrypt_decrypt('decrypt',$i->msg,get_salt($i->to,$i->from)))).'</div>';
			$out .= (($type == 'in') ? '<div class="msg_links"><span class="reply">Reply</span>'.
						($i->type=='c'?'<a href="'.get_comment_link(encrypt_decrypt('decrypt',$i->msg,get_salt($i->to,$i->from))).'">Link</a>':'').
						'</div>
					<form class="reply_form" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="to" value="'.$i->from.'">
						<input type="hidden" name="type" value="'.$i->type.'">
						<input type="hidden" name="pmid" value="'.$i->id.'">'.
						($i->type=='c'?'<input type="hidden" name="cid" value="'.encrypt_decrypt('decrypt',$i->msg,get_salt($i->to,$i->from)).'">':'').
						'<textarea name="msg"></textarea>
						<br>
						<input class="fat-button" type="submit" name="submit" value="Reply">':'').'
					</form>';
		$out .= "</div>";
	}
	return $out;
}
add_action( 'comment_post', 'pm_me_ur_comment');
function pm_me_ur_comment ($comment_ID){
	$c = get_comment($comment_ID);
	if($c->comment_parent) {
		$t = get_comment($c->comment_parent);
		$to = $t->user_id;
	} else {
		$t = get_post($c->comment_post_ID);
		$to = $t->post_author;
	}
	feddit_send_PM(array('to' => $to, 'msg' => $comment_ID, 'type' => 'c'));
}
function get_admin_new($u){
	if ($get = get_option('feddit_admin_read','')){
		$read = explode(',',get_option('feddit_admin_read',''));
		$new = true;
		foreach($read as $r){
			if ($u == $r){
				$new = false;
				break;
			}
		}
	} else {
		$new = false;
	}
	return $new;
}
function encrypt_decrypt($action, $string, $salt="") {
    $output = false;
    $encrypt_method = "AES-256-CBC";
	$id = get_current_user_id();
    $secret_key = 'ShmukliDoesntKnowWhatFunIs83431~'.$salt;
    $secret_iv = 'PartyFavorsIsWhatTPWGivesInTheBathroom732292~'.$salt;
    // hash
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    }
    else if( $action == 'decrypt' ){
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}
function get_salt ($u1,$u2){
	$u=get_current_user_id();
	if($u == $u1 || $u == $u2 || current_user_can('administrator')){
		if($u1 > $u2) {
			return $u2.$u1;
		} else {
			return $u1.$u2;
		}
	}
}