<?php
add_shortcode('feddit_modlog', 'feddit_modlog_page');
function feddit_modlog_page(){
	$out = "";
	global $wpdb;
	$sql='SELECT * FROM '.$wpdb->prefix . 'msg WHERE `type` = \'l\' ORDER BY msg_date DESC;';
	$input = $wpdb->get_results( $sql , OBJECT );
	$out .= display_modlog($input);
	$wpdb->update($wpdb->prefix .'msg', array('read' => 1), array('to' => get_current_user_id()));
	return $out;
}
function display_modlog($input) {
	$out="";
	foreach ($input as $i) {
		$out .= "<div class=\"feddit_modlog\">";
			$f = ($type == 'in') ? get_userdata($i->from) : get_userdata($i->to);
			$out .= '<div class="modlog_head">'.feddit_return_author($f->ID).' <span title="'.preg_replace("/ /",", ",date("Y-m-d G:i:s",$i->msg_date)).' GMT">'.time_elapsed_string(date("Y-m-d G:i:s",$i->msg_date)).'</span></div>';
			$out .= '<div class="modlog_msg">'.encrypt_decrypt('decrypt',$i->msg,get_salt($i->to,$i->from)).'</div>';
		$out .= "</div>";
	}
	return $out;
}
function add_modlog($type, $id) {
	$u = wp_get_current_user();
	switch ($type) {
		case 'd_post':
			$p = get_post($id);
			$t = get_userdata($p->post_author);
			$msg = 'Deletion: '.$u->user_login.' deleted post \''.$p->ID.'\' by '.$t->user_login.' on '.date('D j M Y H:i:s'); 
			feddit_send_PM(array('to' => $p->post_author, 'msg' => $msg, 'type' => 'l'));
			break;
		case 'd_comment':
			$c = get_comment($id);
			$t = get_userdata($c->user_id);
			$msg = 'Deletion: '.$u->user_login.' deleted comment \''.$c->comment_ID.'\' by '.$t->user_login.' on '.date('D j M Y H:i:s'); 
			feddit_send_PM(array('to' => $c->user_id, 'msg' => $msg, 'type' => 'l'));
			break;
		case 'flair':
			$t = get_userdata($id);
			$msg = 'Flair: '.$u->user_login.' granted '.$t->user_login.' a new flair \''.get_the_author_meta("flair",$id).'\' on '.date('D j M Y H:i:s'); 
			feddit_send_PM(array('to' => $id, 'msg' => $msg, 'type' => 'l'));
			break;
		case 'ban':
		case 'unban':
			$t = get_userdata($id);
			$msg = '<b>Ban:</b> '.$u->user_login.' '.$type.'ned '.$t->user_login.' on '.date('D j M Y H:i:s'); 
			feddit_send_PM(array('to' => $id, 'msg' => $msg, 'type' => 'l'));
			break;
	}
}
function get_feddit_user_notes($id) {
	$out = '';
	global $wpdb;
	if(current_user_can('delete_others_feddits')){
		if($input = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix . 'msg WHERE `to` = '.$id.' AND (`type` = \'n\' OR `type` = \'l\') ORDER BY msg_date DESC;', OBJECT )){
			$out .= '<span class="modnotesbtn">[+'.count($input).']</span>';
			$out .= '<div class="modnotes">';
			$out .= '<div class="modnoteslist">';
				$o = 0;
				$op = get_option("feddit_modnotes_num", 5);
				foreach ($input as $i) {
					$out .= '<span title="'.date('D j M Y H:i:s',$i->msg_date).'">'.encrypt_decrypt('decrypt',$i->msg,get_salt($i->to,$i->from)).'</span><br>';
					$o++;
					if($o == $op){break;}
				}
			$out .= '</div>';
		} else {
			$out .= '<span class="modnotesbtn">[+]</span>';
			$out .= '<div class="modnotes">';
		}
		$out .= '<input class="usernoteinput" autocomplete="off" type="text"><span class="admin_func" post_id="0" data-user="'.$id.'" nonce="'. wp_create_nonce("admin_nonce").'" action="add_note">Add note</span>';
		$out .= '</div>';
	}
	return $out;
}