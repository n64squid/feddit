<?php
add_action("wp_ajax_remove_post", "post_remove");
function post_remove() {
	if ( !wp_verify_nonce($_REQUEST['nonce'], "admin_nonce")) {
		$result['type'] = "nonce";
	} else if (current_user_can('delete_others_feddits')) {
		if ($_REQUEST['t']=='p'){
			$p = get_post($_REQUEST['id']);
			$p->post_status = "removed";
			wp_update_post($p);
			add_modlog('d_post', $_REQUEST['id']);
		} else if ($_REQUEST['t']=='c'){
			add_modlog('d_comment', $_REQUEST['id']);
			update_comment_meta($_REQUEST['id'],"report_status","removed");
		}
		$result['type'] = "success";
		$result['color'] = "#fcc";
	}
	die(json_encode($result));
}
add_action("wp_ajax_approve_post", "post_approve");
function post_approve() {
	if ( !wp_verify_nonce($_REQUEST['nonce'], "admin_nonce")) {
		$result['type'] = "nonce";
		exit(json_encode($result));
	} else if (current_user_can('delete_others_feddits')) {
		if ($_REQUEST['t']=='p'){
			$p = get_post($_REQUEST['id']);
			$p->post_status = "publish";
			wp_update_post($p);
		} else if ($_REQUEST['t']=='c'){
			delete_comment_meta($_REQUEST['id'], "report_status");
		}
		$result['type'] = "success";
		$result['color'] = "#cfc";
		$result = json_encode($result);
	}
	die(json_encode($result));
}
add_action( 'wp_ajax_ban_user', 'ban_user' );
function ban_user() {
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "admin_nonce")) {
		$result["type"] = "noncefail";
	} else if (current_user_can('delete_others_feddits')) {
		if (!get_user_meta($_REQUEST['user_id'],"ban")){
			update_user_meta($_REQUEST['user_id'],"ban",1);
			$result["ban"] = "ban";
		} else {
			delete_user_meta($_REQUEST['user_id'],"ban");
			$result["ban"] = "unban";
		}
		$result["type"] = "success";
		add_modlog($result["type"], $_REQUEST['user_id']);
	}
	die(json_encode($result));
}
add_action('wp_ajax_add_note', 'add_note' );
function add_note() {
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "admin_nonce")) {
		$result["type"] = "noncefail";
	} else if (current_user_can('delete_others_feddits')) {
		$u=get_userdata(get_current_user_id());
		feddit_send_PM(array('to' => $_REQUEST['user_id'], 'msg' => '<b>'.$u->user_login.':</b> '.$_REQUEST['msg'], 'type' => 'n'));
		$result['type'] = "success";
	}
	die(json_encode($result));
}
add_action("wp_ajax_send_report", "send_report");
function send_report() {
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "report_nonce")) {
		$result['type'] = "nonce";
		exit(json_encode($result));
	}
	//$_REQUEST['rep']
	$u = wp_get_current_user();
	if($_REQUEST['t']=="p"){
		$re = get_post_meta($_REQUEST['id'],"reports");
		$reports = json_decode($re[0],true);
		if(isset($reports[$u->user_login])){
			$result['type'] = "exists";
		} else {
			$p = get_post($_REQUEST['id']);
			$p->post_status = "reported";
			wp_update_post($p);
			$reports[$u->user_login] = $_REQUEST['rep'];
			update_post_meta($_REQUEST['id'],"reports",json_encode($reports));
			$result['type'] = "success";
		}
	} else if($_REQUEST['t']=="c") {
		$re = get_comment_meta($_REQUEST['id'],"reports");
		$reports = json_decode($re[0],true);
		if(isset($reports[$u->user_login])){
			$result['type'] = "exists";
		} else {
			$c = get_comment($_REQUEST['id']);
			$reports[$u->user_login] = $_REQUEST['rep'];
			update_comment_meta($_REQUEST['id'],"reports",json_encode($reports));
			update_comment_meta($_REQUEST['id'],"report_status","reported");
			$result['type'] = "success";
		}
	}
	
	$result = json_encode($result);
	die($result);
}
add_action("wp_ajax_post_save", "post_save");
function post_save() {
	if ( !wp_verify_nonce($_REQUEST['nonce'], "save_nonce")) {
		$result['type'] = "nonce";
		exit(json_encode($result));
	}
	$u = wp_get_current_user();
	if($arr = get_user_meta($u->ID,'saved')){
		$ar = json_decode($arr[0],true);
		foreach ($ar as $a){
			if ($a['p'] == $_REQUEST['id']) {
				$result['type'] = "exists";
				die(json_encode($result));
			}
		}
		array_push($ar,array('t' => $_REQUEST['t'],'p' => $_REQUEST['id']));
		$result['saved posts'] = $ar;
		update_user_meta($u->ID,'saved', json_encode($ar));
	} else {
		update_user_meta($u->ID,'saved', json_encode(array(array('t' => $_REQUEST['t'],'p' => $_REQUEST['id']))));
	}
	$result['type'] = "success";
	die(json_encode($result));
}
function post_funcs() {
    if (isset($_POST['submit'])) {
		global $wpdb;
		if(is_user_logged_in()){
			if($_POST['submit'] == "Change Password"){
				global $alert;
				$old = $_POST['old_p'];
				$new = $_POST['new_p'];
				$user = wp_get_current_user();
				if ( $user && wp_check_password( $old, $user->data->user_pass, $user->ID) ){
					wp_set_password($new, $user->ID);
					$_GET["r"]="pass_set";
				} else {
					$_GET["r"]="bad_pass";
				}
			} else if($_POST['submit'] == "Send Message"){
				if(is_user_logged_in()){
					$user = (get_user_by('login',$_POST['to'])) ? get_user_by('login',$_POST['to']) : ((get_user_by('slug',$_POST['to'])) ? get_user_by('slug',$_POST['to']) : false);
					if($user){
						feddit_send_PM(array('to' => $user->ID, 'msg' => $_POST['msg'], 'type' => 'p'));
					} else if ($_POST['to'] == 'modmail'){
						feddit_send_PM(array('to' => 0, 'msg' => $_POST['msg'], 'type' => 'm'));
					} else {
						$_GET["r"] = 'bad_pm';
						$_GET["tab"] = 'compose';
					}
				}
			} else if($_POST['submit'] == "Reply"){
				switch ($_POST['type']){
					case "m":
						$wpdb->update($wpdb->prefix .'msg', array('read' => 1), array('id' => $_POST['pmid']));
					case "p":
					case "a":
						if(get_user_by('ID',$_POST['to'])){
							feddit_send_PM(array('to' => $_POST['to'], 'msg' => $_POST['msg'], 'type' => 'p'));
						}
						break;
					case "c":
						$c=get_comment($_POST['cid']);
						$comment_id = wp_new_comment(array(
							'comment_post_ID' => $c->comment_post_ID,
							'comment_content' => $_POST['msg'],
							'comment_parent' => $c->comment_ID,
							'user_id' => get_current_user_id(),
						));
						break;
					// Other cases for the reply button ()
				}
			} else if($_POST['submit'] == "Send mass PM"){
				if ($_POST['masspm']){
					admin_notices('<b>Mass PM sent:</b><br>'.$_POST['masspm']);
					$wpdb->update($wpdb->prefix .'msg', array('read' => 1), array('type' => 'a'));
					feddit_send_PM(array('to' => 0, 'msg' => $_POST['masspm'], 'type' => 'a', 'read' => 0));
					update_option('feddit_admin_date',time());
					update_option('feddit_admin_read','');
				} else {
					admin_notices('You need a message to send!','error');
				}
			} else if ($_POST['submit'] == "View inboxes") {
				
				if($_POST['to1']!='' && $_POST['to2']!='') {
					$user1 = (get_user_by('login',$_POST['to1'])) ? get_user_by('login',$_POST['to1']) : ((get_user_by('slug',$_POST['to1'])) ? get_user_by('slug',$_POST['to1']) : false);
					$user2 = (get_user_by('login',$_POST['to2'])) ? get_user_by('login',$_POST['to2']) : ((get_user_by('slug',$_POST['to2'])) ? get_user_by('slug',$_POST['to2']) : false);
					if ($user1 && $user2) {
						$sql='SELECT * FROM '.$wpdb->prefix . 'msg WHERE (`to` = '.$user1->ID.' AND `from` = '.$user2->ID.') OR (`to` = '.$user2->ID.' AND `from` = '.$user1->ID.') ORDER BY msg_date DESC;';
						$input = $wpdb->get_results( $sql , OBJECT );
						$GLOBALS['pms'] = display_pm($input,'in');
						
						$user = wp_get_current_user();
						$msg = 'Hello. Due to an administration reason (reports, security, etc.), '.$user->user_login.' has decrypted the conversations between '.$user1->user_login.' and '.$user2->user_login.' due to an administration reason. This is an automated message sent by the feddit system in compliance with our transparency policy.';
						feddit_send_PM(array('to' => $user1->ID, 'msg' => $msg, 'type' => 'p'));
						feddit_send_PM(array('to' => $user2->ID, 'msg' => $msg, 'type' => 'p'));
					} else {
						admin_notices('There has been an error locating the users. Make sure you typed it in correctly.','error');
					}
				} else {
					admin_notices('Enter a name into each box!','error');
				}
			} else if ($_POST['submit'] == "Change Flair") {
				$_REQUEST['flair'] = $_POST['flair'];
				$_REQUEST['nonce'] = wp_create_nonce("user_flair");
				$_REQUEST['user_id'] = get_current_user_id();
				update_flair();
			}
		}
    }
}
add_action( 'init', 'post_funcs' );
function admin_notices($msg,$type='success') {//success, error, warning, info
	$GLOBALS['adminalert']=$msg;
	$GLOBALS['adminalerttype']=$type;
	add_action( 'admin_notices', function($type){ ?>
		<div class="notice notice-<?php echo $GLOBALS['adminalerttype']; ?> is-dismissible">
			<p><?php echo $GLOBALS['adminalert']; ?></p>
		</div>
		<?php } 
	);
}
?>