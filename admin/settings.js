jQuery(document).ready( function() {
	var working = false;
	jQuery(".flair span").click( function() {
		if (!working){
			jQuery(this).siblings(".flairinput").css("display","inline");
			jQuery(this).siblings(".flairinput").focus();
			jQuery(this).css("display","none");
		}
	});
	jQuery(".flairinput").focusout( function() {
		if (!working){
			user_id = jQuery(this).attr("data-user");
			nonce = jQuery(this).attr("data-nonce");
			flair = jQuery(this).val();
			jQuery(this).siblings("span").css("display","inline");
			jQuery(this).css("display","none");
			jQuery(this).siblings("img").attr("src",plugdir+"/feddit/images/loading.gif");
			jQuery(this).siblings("img").css("display","inline");
			working = true;
			jQuery.ajax({
				type : "post",
				dataType : "json",
				url : ajaxurl,
				data : {action: "update_flair", user_id : user_id, nonce: nonce, flair: flair},
				success: function(response) {
					if(response.type == "success"){
						jQuery("#loading"+user_id).attr("src",plugdir+"/feddit/images/check.png");
						jQuery("#loading"+user_id).siblings("span").html(flair);
					} else if (response.type == "deleted") {
						jQuery("#loading"+user_id).attr("src",plugdir+"/feddit/images/x.png");
						jQuery("#loading"+user_id).siblings("span").html("[not set]");
					} else if (response.type == "same" || response.type == "no difference") {
						jQuery("#loading"+user_id).attr("src",plugdir+"/feddit/images/circle.png");
					} else {
						alert("Something horrible happened.");
					}
					working = false;
					jQuery("#loading"+user_id).delay(4000).slideToggle(500);
				}
			});
		}
	});

});