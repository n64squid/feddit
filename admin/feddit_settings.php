<?php
//////////////////////
// Declare settings //
//////////////////////

function set_feddit_settings() { 
	// On plugin activation
	add_option("feddit_number", 10);
	add_option("short_length", 250);
	add_option("feddit_allow_vote", 'on');
	add_option("feddit_allow_vote_user", '');
	add_option("feddit_allow_submit", 'on');
	add_option("feddit_allow_comment", 'on');
	add_option("feddit_other_report", 'on');
	add_option("feddit_require_approval", 'on');
	add_option("feddit_register_url", '');
	add_option("feddit_modnotes_num", 5);
	
	add_option("alg_init_str", 100);
	add_option("alg_vote_str", 10);
	add_option("alg_decay", 10);
	add_option("exp_buffer", 1000000);
	
	add_option("vote_per_day", 10);
	
	add_option("feddit_description", "");
	add_option("feddit_comment", "Leave a comment");
	add_option("feddit_report", "Spam");
	
	add_option("feddit_enable_msg", "");
	add_option("feddit_admin_date", "0");
	add_option("feddit_admin_read", "");
	
	
}

function initialize_theme_options() {
    // Assign the sections
    add_settings_section(
        'feddit_options_section',  // ID
        'General Options',         // Title
        'feddit_options_callback', // Callback function
        'feddit_options'           // Page
    );
    add_settings_section(
        'feddit_text_section',
        'Text options',
        'feddit_text_callback',
        'feddit_options'
    );
    add_settings_section(
        'feddit_algorithm_section',
        'Feddit Algorithm',
        'feddit_algorithm_callback',
        'feddit_algorithm'
    );
    add_settings_section(
        'feddit_voting_section',
        'Voting',
        'feddit_voting_callback',
        'feddit_algorithm'
    );
    add_settings_section(
        'feddit_user_section',
        'Feddit Users',
        'feddit_users_callback',
        'feddit_users'
    );
    add_settings_section(
        'feddit_queue_section',
        'Feddit Queue',
        'feddit_queue_callback',
        'feddit_queue'
    );
    add_settings_section(
        'feddit_message_section',
        'Feddit Messaging',
        'feddit_message_callback',
        'feddit_message'
    );
    add_settings_section(
        'feddit_modmail_section',
        'Modmail',
        'feddit_modmail_callback',
        'feddit_modmail'
    );
    // Assign idividual settings
	
	// General settings
	$settingname = 'feddit_number';
    add_settings_field( 
        $settingname,
        'Feddits per page',
        'settings_num',
        'feddit_options', 
        'feddit_options_section',
        array(
            'How many Feddits to display each time the user loads.',
			$settingname
        )
    );
	$settingname = 'short_length';
    add_settings_field( 
        $settingname,
        'Short post length',
        'settings_num',
        'feddit_options', 
        'feddit_options_section',
        array(
            'The length (in characters) of post snippets on the user page and elsewhere.',
			$settingname
        )
    );
	$settingname = 'feddit_allow_vote';
    add_settings_field( 
        $settingname,
        'Allow voting',
        'settings_check',
        'feddit_options', 
        'feddit_options_section',
        array(
            'Enable the voting module.',
			$settingname
        )
    );
	$settingname = 'feddit_allow_vote_user';
    add_settings_field( 
        $settingname,
        'Allow user page voting',
        'settings_check',
        'feddit_options', 
        'feddit_options_section',
        array(
            'Allow users to vote when viewing a user\'s posts/comments',
			$settingname
        )
    );
	$settingname = 'feddit_allow_submit';
    add_settings_field( 
        $settingname,
        'Allow posting',
        'settings_check',
        'feddit_options', 
        'feddit_options_section',
        array(
            'Enable the front page submission module.',
			$settingname
        )
    );
	$settingname = 'feddit_allow_comment';
    add_settings_field( 
        $settingname,
        'Allow commenting',
        'settings_check',
        'feddit_options', 
        'feddit_options_section',
        array(
            'Allow users to comment on posts and other comments.',
			$settingname
        )
    );
	$settingname = 'feddit_require_approval';
    add_settings_field( 
        $settingname,
        'Subscriber approval',
        'settings_check',
        'feddit_options', 
        'feddit_options_section',
        array(
            'Feddits submitted by Subscribers are sent to the Queue. Uncheck to allow them to publish immediately.',
			$settingname
        )
    );
	$settingname = 'feddit_register_url';
    add_settings_field( 
        $settingname,
        'Feddit register url',
        'settings_text',
        'feddit_options', 
        'feddit_options_section',
        array(
            'URL where the registration page is located, just the bits after '.site_url(),
			$settingname
        )
    );
	$settingname = 'feddit_modnotes_num';
    add_settings_field( 
        $settingname,
        'Show modnotes',
        'settings_num',
        'feddit_options', 
        'feddit_options_section',
        array(
            'Number of max modnotes to show.',
			$settingname
        )
    );
	
	// Text settings
	$settingname = 'feddit_description';
    add_settings_field( 
        $settingname,
        'Feddit description',
        'settings_text',
        'feddit_options', 
        'feddit_text_section',
        array(
            'Text to include on the Feddit page (Optional)',
			$settingname
        )
    );
	$settingname = 'feddit_comment';
    add_settings_field( 
        $settingname,
        'Comment text',
        'settings_textarea',
        'feddit_options', 
        'feddit_text_section',
        array(
            'New line delimited. One of these will appear above the Reply box. Colon already included.',
			$settingname
        )
    );
	$settingname = 'feddit_other_report';
    add_settings_field( 
        $settingname,
        'Allow custom report text',
        'settings_check',
        'feddit_options', 
        'feddit_text_section',
        array(
            'With this setting, users can submit their own report text.',
			$settingname
        )
    );
	$settingname = 'feddit_report';
    add_settings_field( 
        $settingname,
        'Report options',
        'settings_textarea',
        'feddit_options', 
        'feddit_text_section',
        array(
            'New line delimited. Predetermined reasons why something could be reported. Useful for data validation.',
			$settingname
        )
    );
	
	// Algorithm settings
	$settingname = 'alg_init_str';
    add_settings_field( 
        $settingname,
        'Initial strength',
        'settings_num',
        'feddit_algorithm', 
        'feddit_algorithm_section',
        array(
            'The initial hotness of a post.',
			$settingname
        )
    );
	$settingname = 'alg_vote_str';
    add_settings_field( 
        $settingname,  
        'Vote Strength', 
        'settings_num',
        'feddit_algorithm',
        'feddit_algorithm_section', 
        array(
            'Increase of hotness with each vote.',
			$settingname
        )
    );
	$settingname = 'alg_decay';
    add_settings_field( 
        $settingname,
        'Decay coefficient',
        'settings_num',
        'feddit_algorithm', 
        'feddit_algorithm_section',
        array(
            'The rate of decay over time. (100 minimum, higher numbers make hotness drop faster)',
			$settingname
        )
    );
	$settingname = 'exp_buffer';
    add_settings_field( 
        $settingname,
        'Exponent buffer',
        'settings_num',
        'feddit_algorithm', 
        'feddit_algorithm_section',
        array(
            'A really large number to balance the inverse exponent and prevent rounding errors to 0. No touchy please.',
			$settingname
        )
    );
	
	$settingname = 'vote_per_day';
    add_settings_field( 
        $settingname,
        'Daily votes',
        'settings_num',
        'feddit_algorithm', 
        'feddit_voting_section',
        array(
            '[Not implemented yet] Votes allowed per user per day',
			$settingname
        )
    );
	
	// Messaging settings
	$settingname = 'feddit_enable_msg';
    add_settings_field( 
        $settingname,
        'Enable messaging',
        'settings_check',
        'feddit_message', 
        'feddit_message_section',
        array(
            'Enable/disable the messaginge service.',
			$settingname
        )
    );
     
    // Finally, we register the fields with WordPress
	
	register_setting('feddit_options','feddit_number');
	register_setting('feddit_options','short_length');
	register_setting('feddit_options','feddit_allow_vote');
	register_setting('feddit_options','feddit_allow_vote_user');
	register_setting('feddit_options','feddit_allow_submit');
	register_setting('feddit_options','feddit_allow_comment');
	register_setting('feddit_options','feddit_require_approval');
	register_setting('feddit_options','feddit_register_url');
	register_setting('feddit_options','feddit_modnotes_num');
    
    register_setting('feddit_algorithm','alg_init_str');
	register_setting('feddit_algorithm','alg_vote_str');
    register_setting('feddit_algorithm','alg_decay');
    register_setting('feddit_algorithm','exp_buffer');
    
    register_setting('feddit_voting','vote_per_day');
	
	register_setting('feddit_options','feddit_description');
	register_setting('feddit_options','feddit_comment');
	register_setting('feddit_options','feddit_report');
	register_setting('feddit_options','feddit_other_report');
	
	register_setting('feddit_message','feddit_enable_msg');
     
} // end initialize_theme_options
add_action('admin_init', 'initialize_theme_options');
 
/* ------------------------------------------------------------------------ *
 * Section Callbacks
 * ------------------------------------------------------------------------ */
 
function feddit_algorithm_callback() {
    echo '<p>These are the settings to determine how the algorithm is ordered.</p>
	<p>Note: After a certain amount of downvotes (Init_str / Vote_str), the Feddit will be pushed to the bottom of the queue.
	<br>Current downvotes required for removal: '.ceil(get_option("alg_init_str", 100)/get_option("alg_vote_str", 10)).'.</p>
	<p><img src="'.plugins_url().'/feddit/images/algorithm.png"></p>';
}  
function feddit_voting_callback() {
    echo '<p>Voting settings</p>';
} 
function feddit_options_callback() {
    echo '<p>Some general settings for Feddit.</p>';
} 
function feddit_text_callback() {
    echo '<p>Change some of the texts on the website.</p>';
} 
function feddit_users_callback() {
	include ("user.php");
} 
function feddit_queue_callback() {
	include ("queue.php");
} 
function feddit_modmail_callback() {
	include ("modmail.php");
} 
function feddit_message_callback() {
} 
 
/* ------------------------------------------------------------------------ *
 * Field Callbacks
 * ------------------------------------------------------------------------ */
 
function settings_check($args) {
    $html = '<input type="checkbox" id="'.$args[1].'" name="'.$args[1].'" '.checked(get_option($args[1]),'on', false).' />'; 
    $html .= '<label for="'.$args[1].'"> '  . $args[0] . '</label>'; 
    echo $html.false;
} 
function settings_text($args) {
    $html = '<input type="text" id="'.$args[1].'" name="'.$args[1].'" value="'.get_option($args[1]).'" />'; 
    $html .= '<label for="'.$args[1].'"> '  . $args[0] . '</label>'; 
    echo $html.false;
}
function settings_num($args) {
    $html = '<input type="number" step="any" id="'.$args[1].'" name="'.$args[1].'" value="' . get_option($args[1]) . '" /><br>'; 
    $html .= '<label for="'.$args[1].'"> '  . $args[0] . '</label>'; 
    echo $html;
}
function settings_textarea($args) {
    $html = $args[0].'<br>';
    $html .= '<textarea id="'.$args[1].'" name="'.$args[1].'">' . get_option($args[1]) . '</textarea>';
    echo $html;
}
function settings_curve($args) {
	$html = '<input type="radio" name="'.$args[1].'" value="none" '.checked(get_option($args[1]),'none', false).'>None<br>';
	$html .='<input type="radio" name="'.$args[1].'" value="line" '.checked(get_option($args[1]),'line', false).'>Linear<br>'; 
	$html .='<input type="radio" name="'.$args[1].'" value="quad" '.checked(get_option($args[1]),'quad', false).'>Quadratic<br>'; 
	$html .='<input type="radio" name="'.$args[1].'" value="sqrt" '.checked(get_option($args[1]),'sqrt', false).'>Square root<br>'; 
    $html .= $args[0]; 
    echo $html;
}

add_action( 'wp_ajax_update_flair', 'update_flair' );
//add_action( 'wp_ajax_nopriv_update_flair', 'update_flair' );

function update_flair() {
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "user_flair")) {
		$result["type"] = "noncefail";
	} else {
		if($_REQUEST['flair'] != ""){
			if(update_user_meta($_REQUEST['user_id'],"flair",$_REQUEST['flair'])){
				$result["type"] = "success";
				if (current_user_can('administrator') && wp_get_current_user()!=$_REQUEST['user_id']){
					add_modlog('flair', $_REQUEST['user_id']);
				}
			} else {
				$result["type"] = "same";
			}
		} else {
			if (get_user_meta($_REQUEST['user_id'],"flair")){
				delete_user_meta($_REQUEST['user_id'],"flair");
				$result["type"] = "deleted";
			} else {
				$result["type"] = "no difference";
			}
		}
	}
	if(defined('DOING_AJAX') && DOING_AJAX){
		die(json_encode($result));
	} else {
		$GLOBALS['flair_alert'] = $result["type"];
		$_GET['tab'] = "settings";
		
	}
}

?>
