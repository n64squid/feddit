<?php
add_action( 'admin_bar_menu', 'toolbar_link_to_mypage', 999 );

function toolbar_link_to_mypage( $wp_admin_bar ) {
	$wp_query = new WP_Query(
	array(	'post_type' => 'feddit',
			'nopaging' => true,
			'post_status' => array('draft','reported')
	));
	$comments_query = new WP_Comment_Query;
	$comments = $comments_query->query(array(
		'meta_key' => 'report_status',
		'meta_value' => 'reported',
	));
	$coun = $wp_query->post_count ? " (".($wp_query->post_count+count($comments)).")" : "";
	$args = array(
		'id'    => 'feddit_queue',
		'title' => '<img src="'.plugins_url().'/feddit/images/cake-pencil-gray.png"> Queue'.$coun,
		'href'  => admin_url().'admin.php?page=feddit&tab=feddit_queue',
		'meta'  => array( 'class' => 'my-toolbar-page' ),
		'menu_icon' => 'dashicons-welcome-write-blog'
	);
	$wp_admin_bar->add_node( $args );
}
?>