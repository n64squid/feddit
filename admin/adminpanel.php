<?php
//////////////////
// Menu Options //
//////////////////
function my_plugin_menu() {
	$menu = add_menu_page('Feddit', 'Feddit admin', 'administrator', 'feddit', 'feddit_settings', 'dashicons-fph', 66);
	//add_submenu_page( 'feddit', 'Queue', 'feddit Queue', 'manage_options', 'admin.php?page=feddit&tab=feddit_queue');
	add_action( 'admin_print_styles-' . $menu, 'feddit_css' );
}
// Menu html

add_action('admin_menu', 'my_plugin_menu');
function feddit_settings() {
	echo '<div class="wrap">
	<div id="icon-themes" class="icon32"></div>
	<h2>Feddit Settings</h2>';
	settings_errors();
	$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'feddit_options';
	$navtabs = '<h2 class="nav-tab-wrapper"><a href="?page=feddit&tab=feddit_options" class="nav-tab ';
	$navtabs .= ($active_tab == 'feddit_options') ? 'nav-tab-active' : '';
	$navtabs .= '">Feddit Options</a><a href="?page=feddit&tab=feddit_algorithm" class="nav-tab ';
	$navtabs .= ($active_tab == 'feddit_algorithm') ? 'nav-tab-active' : '';
	$navtabs .='">Algorithm options</a><a href="?page=feddit&tab=feddit_users" class="nav-tab ';
	$navtabs .= ($active_tab == 'feddit_users') ? 'nav-tab-active' : '';
	$navtabs .='">User Admin</a><a href="?page=feddit&tab=feddit_queue" class="nav-tab ';
	$navtabs .= ($active_tab == 'feddit_queue') ? 'nav-tab-active' : '';
	$navtabs .='">Mod Queue</a><a href="?page=feddit&tab=feddit_message" class="nav-tab ';
	$navtabs .= ($active_tab == 'feddit_message') ? 'nav-tab-active' : '';
	$navtabs .='">Messaging</a><a href="?page=feddit&tab=feddit_modmail" class="nav-tab ';
	$navtabs .= ($active_tab == 'feddit_modmail') ? 'nav-tab-active' : '';
	$navtabs .='">Modmail</a></h2>';
	echo $navtabs;
	
	if($active_tab == "feddit_users" || $active_tab == "feddit_queue" || $active_tab == "feddit_modmail"){
		do_settings_sections($active_tab);
	} else if ($active_tab == "feddit_message") {
		include ('message.php');
	} else {
		echo '<form method="post" action="options.php">';
		settings_fields($active_tab);
		do_settings_sections($active_tab);
		submit_button();
		echo '</form>';
	}
	echo '</div>';
} 


// Hide admin panel for all but admins, and redirect 
add_action('after_setup_theme', 'remove_admin_bar');
add_action( 'admin_init', 'redirect_non_admin_users' );
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}
function redirect_non_admin_users() {
	if ( !current_user_can('administrator') && !preg_match("/\/wp-admin\/admin-ajax\.php/",$_SERVER['PHP_SELF']) && !preg_match("/\/wp-admin\/admin-post\.php/",$_SERVER['PHP_SELF'])) {
		wp_redirect( home_url() );
		exit;
	}
}
include ("feddit_settings.php");
include ("bar.php");
?>