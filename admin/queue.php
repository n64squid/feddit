<?php
$arr = get_defined_vars();



echo '<div class="div_feddit">';
	$loop = new WP_Query(
	array(	'post_type' => 'feddit',
			'nopaging' => true,//'posts_per_page' => get_option("feddit_number", 10),
			'post_status' => array('draft','reported')
	));
	while ( $loop->have_posts() ) : $loop->the_post(); 
		$post = $GLOBALS["post"];
		echo display_feddit($post->ID,"p",false);
	endwhile; 
	wp_reset_query();
	
	$comments_query = new WP_Comment_Query;
	$comments = $comments_query->query(array(
		'meta_key' => 'report_status',
		'meta_value' => 'reported',
	));
	foreach ( $comments as $comment ) {
		echo display_feddit($comment->comment_id,"c",false);
    }
echo '<div>';
?>