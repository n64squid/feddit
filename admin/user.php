<?php
echo '<div class="users_feddit">';
	echo '<p>These are the user settings for feddit.</p>';
	$blogusers = get_users( '' );
	// Array of WP_User objects.
	echo "<table class=\"usertable\">";
		echo "<tr class=\"usertablehead\">";
			echo '<td>ID</td>';
			echo '<td>Username</td>';
			echo '<td>Role</td>';
			echo '<td>Flair</td>';
			echo '<td>Notes</td>';
			echo '<td>Ban</td>';
		echo "</tr>";
		foreach ( $blogusers as $user ) {
			echo "<tr>";
				echo '<td>' . esc_html($user->ID) . '</td>';
				echo '<td>' . esc_html($user->user_login) . '</td>';
				echo '<td>' . esc_html($user->roles[0]);
				if($user->roles[1]){
					echo ' [+]';
				}
				echo '</td>';
				if(!$flair = get_user_meta($user->ID,"flair",true)){
					$flair = "[not set]";
				}
				$nonce = wp_create_nonce("user_flair");
				echo '<td class="flair"><span>'.$flair.'</span><input data-nonce="'.$nonce.'" data-user="'.$user->ID.'" id="useredit'.$user->ID.'" class="flairinput" value="'.$flair.'"> <img class="loading" id="loading'.$user->ID.'" /></td>';
				echo '<td class="notecol">'.get_feddit_user_notes($user->ID).'</td>';
				$nonce = wp_create_nonce("admin_nonce");
				echo '<td><input class="admin_func" action="ban_user" type="submit" data-user="'.$user->ID.'" name="ban" nonce="'.$nonce.'" value="'.((!get_user_meta($user->ID,"ban"))?'Ban':'Unban').'"><img class="loading" id="loading'.$user->ID.'" /><div class="after_mod"></div></td>';
			echo "</tr>";
		}
	echo "</table>";
echo '<div>';
?>