<?php

echo '<p>Change the messaging options and send out global messages to all users</p>';
if (isset($GLOBALS['pms'])){
	echo '<hr>';
	echo '<h2>PM results between '.$_POST['to1'].' and '.$_POST['to2'].'</h2>';
	echo $GLOBALS['pms'];
}
echo '<hr>';
echo '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">';
	settings_fields($active_tab);
	echo '<h2>Send global PM</h2>';
	echo '<p>Fill this out and press \'Save Changes\' to send a PM that all users can read.</p>';
	if ($_POST['submit'] == "Send mass PM") {
		echo $_POST['pmalert'];
	}
	echo '<textarea class="admintextarea" name="masspm"></textarea><br><br>';
	echo '<input name="submit" id="submit" class="button button-primary" value="Send mass PM" type="submit">';
echo '</form>';
echo '<hr>';
echo '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">';
	settings_fields($active_tab);
	echo '<h2>View user\'s inbox</h2>';
	echo '<p>Write the username of the users you want to see the conversations of.<br><strong>Note:</strong> This will send both users a PM notifying them that you\'ve seen their inbox.</p>';
	echo '<input type="text" name="to1" />';
	echo '<input type="text" name="to2" /><br><br>';
	echo '<input name="submit" id="submit" class="button button-primary" value="View inboxes" type="submit">';
echo '</form>';
echo '<hr>';
echo '<form method="post" action="options.php">';
	settings_fields($active_tab);
	do_settings_sections($active_tab);
	submit_button();
echo '</form>';