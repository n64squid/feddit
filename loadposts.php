<?php
add_shortcode('feddit_labels', 'show_feddit_labels');
function show_feddit_labels(){
	global $labels;
	echo "<p class=\"label-list\">";
	if($labels[0]){ // Top page labels
		foreach ($labels as $l) {
			echo "<span class=\"content_taxa\" tag=\"".$l->slug."\">".$l->name."</span>";
		}
	}
	echo "</p><p>".get_option("feddit_description", "")."</p>";
}
add_shortcode('feddit_loop', 'feddit_loop');
function feddit_loop($offset = 0, $excludestr = ""){
	$exclude = explode(",", $excludestr);
	$out = "";
	// Load algorithm
	add_filter('posts_groupby', 'feddit_algorithm_group');
	add_filter('posts_join_paged', 'feddit_algorithm_join');
	add_filter('posts_orderby', 'feddit_algorithm_order');
	if ($offset){ // AJAX Load
		$i=0;
		foreach($exclude as $e){
			if($e==""){
				//unset($exclude[$i]);
			}
			$i++;
		}
		$loop = new WP_Query( 
		array(	'post_type' => 'feddit',
				'posts_per_page' => get_option("feddit_number", 10),
				'post__not_in' => $exclude
		));
		//$out.="AJAX ".json_encode($exclude);
		
	foreach ($_SESSION["posted"] as $p){
		$out .= $p.', ';
	}
	} else { // initial load
		$loop = new WP_Query(
		array(	'post_type' => 'feddit',
				'posts_per_page' => get_option("feddit_number", 10)
		));
		//$out.="Init";
	}
	
	
	while ( $loop->have_posts() ) : $loop->the_post(); 
	$post = $GLOBALS["post"];
	array_push($exclude,$post->ID);
	$out .= display_feddit($post->ID,"p",get_option("feddit_allow_vote", "on") == "on");
	endwhile; 
	wp_reset_query(); 
	if ($offset){
		return array('out' => $out, 'exclude' => implode(",",$exclude));
	} else {
		$out .= '<div id="more_posts" class="fat-button" data-nonce="'.wp_create_nonce("load_nonce").'" data-category="'.esc_attr($cat_id).'">Load more</div>
		<script>var exclude = "'.implode(",",$exclude).'";</script>';
		echo $out;
		//echo $loop->request;
	}
}

add_action("wp_ajax_load_fat_posts", "load_fat_posts_func");
add_action("wp_ajax_nopriv_load_fat_posts", "load_fat_posts_func");

function load_fat_posts_func (){
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "load_nonce")) {
		$result['type'] = "nonce";
		$result['nonce'] = $_REQUEST['nonce'];
		exit(json_encode($result));
	}  
	$return = feddit_loop($_REQUEST['offset'], $_REQUEST['exclude']);
	$result['code'] = $return['out'];
	$result['exclude'] = $return['exclude'];
	$result = json_encode($result);
	die($result);
}
// Algorithm start

/*
get_option("alg_init_str", 100);
get_option("alg_vote_str", 10);
get_option("alg_decay", 10);
*/
function feddit_algorithm_join (){
	global $wpdb;
	$join = "LEFT JOIN ".$wpdb->prefix."votes ON ".$wpdb->prefix."posts.ID = ".$wpdb->prefix."votes.post";
	return $join;
}
function feddit_algorithm_order (){
	global $wpdb;
	// (Init+(votestrength*votesum))
	$order = "(".get_option("alg_init_str", 100)."+(".get_option("alg_vote_str", 10)."*COALESCE(SUM(".$wpdb->prefix."votes.upvote),0)))";
	$order.="*";
	$order.= "(POWER(".(get_option("alg_decay", 100)/100).",".(-1/get_option("exp_buffer", 1000000))."*(UNIX_TIMESTAMP()-UNIX_TIMESTAMP(".$wpdb->prefix."posts.post_date_gmt))))";
	$order.=" DESC";
	return $order;
}
function feddit_algorithm_group (){
	global $wpdb;
	$group = $wpdb->prefix."posts.ID";
	return $group;
}
function display_feddit($id,$typ,$voting,$buttons = array("comments", "report", "save", "mod")){
	$out="";
	$can = current_user_can('delete_others_feddits');
	if($typ=="p"){
		$post =  get_post($id);
		$feddit_title_prefix = "";
		$feddit_title = $post->post_title;
		$feddit_author = feddit_return_author($post->post_author);
		$feddit_author_id = $post->post_author;
		$feddit_state = $post->post_status;
		$feddit_link = get_post_meta($id,"_url",true);
		$feddit_perma = $post->guid;
		$feddit_date = $post->post_date_gmt;
		$taxa = get_the_terms($id,"label") ?  get_the_terms($id,"label") : (get_the_terms($id,"post_tag") ?  get_the_terms($id,"post_tag") : array("")); 
	} else {
		$com = get_comment($id);
		$par = get_post($com->comment_post_ID);
		$feddit_title_prefix = "Comment on: ";
		$feddit_title = $par->post_title;
		$feddit_author = feddit_return_author($par->post_author);
		$feddit_author_id = $par->post_author;
		$feddit_state = $par->post_status;
		$feddit_perma = $par->guid."#li-comment-".$id;
		if(!$feddit_link = get_post_meta($com->comment_post_ID,"_url",true)){$feddit_link = $feddit_perma;}
		$feddit_date = $com->comment_date_gmt;
		$taxa[0]="";
		//li-comment-6
	}
	$feddit_date = '<span title="'.preg_replace("/ /",", ",$feddit_date).' GMT">'.time_elapsed_string($feddit_date).'</span>';
	$nonce = wp_create_nonce("my_user_vote_nonce");
	$uid = get_current_user_id();
	$out .= '<div class="fpost';
	foreach( $taxa as $l){ 
		$out .= " class-".$l->slug;
	}
	$out .= '" id="fpost_'.$id.'">';
		if($voting){ 
			$out .= '<div class="votediv">';
				$out .= '<img class="upvote vote" id="upvote'.$id.'" vote="up" type="p" data-nonce="' . $nonce . '" data-post_id="' . $id . '" src="'.get_vote_img(true,$id,$uid,"p").'" /><br />';
				 $out .= get_post_votes($id,"p")."<br />"; 
				 $out .= '<img class="upvote vote" id="downvote'.$id.'" vote="down" type="p" data-nonce="' . $nonce . '" data-post_id="' . $id . '" src="'.get_vote_img(false,$id,$uid,"p").'" />';
			$out .= '</div>';
		} 
	$out .= '<div class="fpost-content">';
		$out .= '<div class="fpost-head">';
		$score = get_post_votes($id,"p");
		$score = ($score > 0) ? "[+".$score."]" : "[".$score."]";
		$out .= $feddit_title_prefix.'<a class="fpost-title" href="'.$feddit_link.'">'.$feddit_title.'</a> 
					<span class="post_author">by '.$feddit_author.'</span>'.get_feddit_user_notes($feddit_author_id).'
					<span class="post_score">'.$score.'</span>'; 
		$out .= '</div>';
		$out .= '<div class="feddit-labels">'.$feddit_date." | ";
		if($typ == "p") {
			
			if($taxa[0]){
				$out .= 'Tags: ';
				foreach( $taxa as $l){ 
					$out .= "<span class=\"fpost_taxa\" tag=\"".$l->slug."\">".$l->name."</span>";
				}
			} else {
				$out .= 'No Tags';
			}
		} else {
			$out.= (substr($com->comment_content,0,get_option("short_length", 250))==$com->comment_content) ? $com->comment_content : substr($com->comment_content,0,get_option("short_length", 250))."...";
		}
		$out .= '</div>';
		$out .= feddit_post_buttons($buttons,array("id" => $id, "state" => $feddit_state, "t" => $typ, "perma" => $feddit_perma, "auth" => $feddit_author_id));
		
		//$out .= feddit_post_drop(array("report","mod"),array("id" => $id, "state" => $feddit_state, "t" => $typ));
		$out .= '</div>';
	$out .= '</div>';
return $out;
}
function feddit_post_buttons($arr,$args){
	$out='<div class="feddit-links">';
	foreach($arr as $a){
		switch ($a) {
			case "comments":
				$out .= '<a href="'.$args['perma'].'">Comments</a>';
				break;
			case "reply":
				
				$out .= '<a rel="nofollow" class="comment-reply-link" href="http://localhost/fphsite/hello-world/?replytocom='.get_comment_ID().'#respond" onclick=\'return addComment.moveForm( "comment-'.get_comment_ID().'", "'.get_comment_ID().'", "respond", "'.get_the_ID().'" )\' aria-label="Reply to Example">Reply</a>';
				break;
			case "report":
				$out .= '<a class="report_btn">Report</a>';
				break;
			case "save":
				$out .= '<a class="save_btn" nonce="'.wp_create_nonce("save_nonce").'" post_id="'.$args['id'].'" type="'.$args['t'].'" action="post_save">Save</a>';
				break;
			case "mod":
				if(current_user_can('delete_others_feddits')){
					$reports = ($args['t'] == "p") ? get_post_meta($args['id'],"reports") : get_comment_meta($args['id'],"reports");
					$reports = json_decode($reports[0],true);
					$out .= '<a class="mod_btn">Mod tools</a>';
					$out .= ($args['state'] == 'reported' || $args['state'][0] == 'reported') ? '<a class="mod_btn"><span class="modalert">('.count($reports).')</span></a>' : '';
				}
				break;
		}
	}
	$out .= '</div>';
	foreach($arr as $a){
		switch ($a) {
			case "report":
				$nonce = wp_create_nonce("report_nonce");
				$out .= '<div class="report_post">';
					foreach(explode(PHP_EOL,get_option("feddit_report", "Spam")) as $ro){
						$out .= '<input type="radio" name="report'.$args['id'].'" id="label-'.$ro.'-'.$args['id'].'" value="'.$ro.'" autocomplete="off">
								 <label for="label-'.$ro.'-'.$args['id'].'"><span>'.$ro.'</span></label>';
					}
					if(get_option("feddit_other_report", false)){
						$out .= '<input type="radio" name="report'.$args['id'].'" id="label-other-'.$args['id'].'" value="Other" autocomplete="off">
								 <label for="label-other-'.$args['id'].'"><span>Other</span></label>';
						$out .= '<input type="text" name="report'.$args['id'].'" value="" autocomplete="off" placeholder="Other">';
					}
					$out .= '<div class="send_report" nonce="'.$nonce.'" post_id="'.$args['id'].'" type="'.$args['t'].'" action="send_report">Send Report</div>';
					$out .= '<div class="after_report"></div>';
				$out .= '</div>';
				break;
			case "mod":
				$nonce = wp_create_nonce("admin_nonce");
				if(current_user_can('delete_others_feddits')){
					$out .= '<div class="mod_tools">';
					$reports = ($args['t'] == "p") ? get_post_meta($args['id'],"reports") : get_comment_meta($args['id'],"reports");
					$reports = json_decode($reports[0],true);
						if(count($reports)){
							$out .= '<div class="modalert">';
							foreach($reports as $k => $r){
								$out .= $k.': '.$r.'<br>';
							}
							$out .= '</div>';
						}
						if($args['t']=="p"){
							$out .= ($args['state']!="removed")?'<span class="admin_func" type="'.$args['t'].'" post_id="'.$args['id'].'" nonce="'.$nonce.'" data-user="'.$args['auth'].'" action="remove_post">Delete</span>':'';
							$out .= ($args['state']!="publish")?'<span class="admin_func" type="'.$args['t'].'" post_id="'.$args['id'].'" nonce="'.$nonce.'" data-user="'.$args['auth'].'" action="approve_post">Approve</span>':'';
						} else if($args['t']=="c"){
							$out .= '<span class="admin_func" type="'.$args['t'].'" post_id="'.$args['id'].'" nonce="'.$nonce.'" data-user="'.$args['auth'].'" action="remove_post">Delete</span>';
							$out .= ($args['state'][0]=="reported"||$args['state'][0]=="removed")?
							'<span class="admin_func" type="'.$args['t'].'" post_id="'.$args['id'].'" nonce="'.$nonce.'" data-user="'.$args['auth'].'" action="approve_post">Approve</span>':'';
						}
						$out .= '<span class="admin_func" post_id="'.$args['id'].'" data-user="'.$args['auth'].'" nonce="'.$nonce.'" action="ban_user">'.((!get_user_meta($args['auth'],"ban"))?'Ban':'Unban').'</span>';
						$out .= '<div class="after_mod"></div>';
					$out .= '</div>';
				}
				break;
		}
	}
	return $out;
}
function feddit_post_drop($arr=array(),$args){
	$out="";
	
	return $out;
}


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
function feddit_return_author ($id) {
	$out = '<a href="'.site_url()."/u/".get_the_author_meta("user_nicename",$id).'/">'.get_the_author_meta("user_login",$id).'</a>';
	if ($f=get_the_author_meta("flair",$post->post_author)) {
		$out .= " [$f]";
	}
	return $out;
}