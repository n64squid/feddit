jQuery(document).ready( function() {
	var voting = false;
	var loading = false;
	var toggleclass = "";
	var i = false;
	var filters = "";
	jQuery("body").on( "click", ".vote", function() {
		if (!voting){
			voting = true;
			post_id = jQuery(this).attr("data-post_id");
			nonce = jQuery(this).attr("data-nonce");
			vote = jQuery(this).attr("vote");
			type = jQuery(this).attr("type");
			jQuery("#"+vote+"vote"+post_id).attr("src",plugdir+"/feddit/images/loading.gif");
			jQuery.ajax({
				type : "post",
				dataType : "json",
				url : ajaxurl,
				data : {action: "user_vote", post_id : post_id, nonce: nonce, vote: vote, type: type},
				success: function(response) {
					if(response.type == "vote"){
						jQuery("#"+vote+"vote"+post_id).attr("src",plugdir+"/feddit/images/"+vote+"voted.png");
					} else if (response.type == "unvote") {
						jQuery("#"+vote+"vote"+post_id).attr("src",plugdir+"/feddit/images/"+vote+"vote.png");
					} else if (response.type == "switch") {
						jQuery("#"+vote+"vote"+post_id).attr("src",plugdir+"/feddit/images/"+vote+"voted.png");
						jQuery("#"+response.opp+"vote"+post_id).attr("src",plugdir+"/feddit/images/"+response.opp+"vote.png");
					} else {
						alert("Something horrible happened."+response.type);
					}
					voting = false;
				}
			});
		}
	});
	jQuery("#fat_login").click( function() {
		jQuery("#fph_login").slideToggle("slow",restoreopacity);
		jQuery("#fph_register").slideUp();
		jQuery("#fat_login").css("opacity","1");
		jQuery("#fat_register").css("opacity","0.3");
	});
	jQuery("#fat_register").click( function() {
		jQuery("#fph_register").slideToggle("slow",restoreopacity);
		jQuery("#fph_login").slideUp();
		jQuery("#fat_register").css("opacity","1");
		jQuery("#fat_login").css("opacity","0.3");
	});
	function restoreopacity(){
		if(jQuery("#fph_register").is( ":hidden" ) && jQuery("#fph_login").is( ":hidden" )){
			jQuery("#fat_register").css("opacity","1"); 
			jQuery("#fat_login").css("opacity","1");
		}
	}
	jQuery("#fat_submit").click( function() {
		jQuery("#fph_submit").slideToggle("slow",restoreopacity);
	});
	jQuery("#labels_label").click( function() {
		jQuery("#labels_list").slideToggle("fast");
	});
	jQuery("#fph_alert img").click( function() {
		jQuery("#fph_alert").remove();
	});
	jQuery("#modmail").click( function() {
		jQuery("#compose_to").val('modmail');
	});
	jQuery(".content_taxa").click( function do_taxa() {
		toggleclass = jQuery(this).attr("tag");
		labels[toggleclass] = !labels[toggleclass];
		jQuery(".fpost").slideUp();
		i = false;
		jQuery.each(labels, function(key, value){
			if(value){
				jQuery(".class-"+key).slideDown();
				i = true;
				jQuery(".content_taxa[tag="+key+"]").css("background-color","#ddd");
			} else {
				jQuery(".content_taxa[tag="+key+"]").css("background-color","#eee");
			}
		});
		if (!i){
			jQuery(".fpost").slideDown();
		}
	});
	function load_posts() {
		nonce = jQuery("#more_posts").attr("data-nonce");
		jQuery("#more_posts").css("display", "none");
		jQuery("#more_posts").after('<img src="'+plugdir+'/feddit/images/loading_big.gif"  id="posts_loading" />');
		loading = true;
		jQuery.ajax({
			type : "post",
			dataType : "json",
			url : ajaxurl,
			data : {action: "load_fat_posts", offset : curr_offset, exclude: exclude, nonce: nonce},
			success: function(response) {
				loading = false;
				jQuery("#posts_loading").remove();
				if(response.code!=""){
					jQuery("#more_posts").css("display", "block");
					jQuery("#more_posts").before(response.code);
					curr_offset += offset;
				} else {
					jQuery("#more_posts").before("No more posts!");
				}
				exclude = response.exclude;
				i = false;
				jQuery.each(labels, function(key, value){
					if(value){
						//jQuery(".class-"+key).slideUp();
						i = true;
						filters += ".class-"+key+", ";
					}
				});
				filters = filters.substring(0, filters.length - 2);
				jQuery(".fpost:nth-of-type(n+"+(curr_offset-offset+1)+")").css("display","none");
				if(i){
					jQuery(filters).slideDown();
				} else {
					jQuery(".fpost:nth-of-type(n+"+(curr_offset-offset+1)+")").slideDown();
				}
			}
		});
	}
	jQuery("#more_posts").click(function(){
		if(!loading){
			load_posts();
		}
	});
	/*jQuery(window).scroll(function() {
		if(jQuery(window).scrollTop() + jQuery(window).height() == jQuery(document).height()) {
			if(!loading && /\/f\/$/.test(window.location.href)){
				load_posts();
			}
		}
	});*/
	jQuery(".mod_btn").click( function() {
		jQuery(this).parent().siblings(".mod_tools").slideToggle("fast");
	});
	jQuery(".report_btn").click( function() {
		jQuery(this).parent().siblings(".report_post").slideToggle("fast");
	});
	jQuery(".msg_links .reply").click( function() {
		jQuery(this).parent().siblings("form.reply_form").slideToggle("fast");
	});
	jQuery(".modnotesbtn").click( function() {
		jQuery(this).siblings(".modnotes").slideToggle("fast");
	});
	jQuery(".send_report").click( function() {
		rep = jQuery(this).siblings("input[type=radio]:checked").val();
		if (rep == "Other"){
			rep = jQuery(this).siblings("input[type=text]").val();
			if (rep == ""){
				rep = "Other";
			}
		}
		if (rep === undefined) {
			jQuery(this).siblings(".after_report").html("Please choose a report reason")
		} else if (!loading){
			nonce = jQuery(this).attr("nonce");
			id = jQuery(this).attr("post_id");
			action = jQuery(this).attr("action");
			loading = true;
			t = jQuery(this).attr("type");
			jQuery(this).siblings(".after_report").html('');
			jQuery(this).siblings(".after_report").css({
			   'background-image' : "url(\""+plugdir+"/feddit/images/loading.gif\")",
			   'background-repeat' : "no-repeat",
			   'background-position' : "center",
			   'float' : 'left',
			   'width' : '28px',
			   'height' : '28px'
			})
			jQuery.ajax({
				type : "post",
				dataType : "json",
				url : ajaxurl,
				data : {action: action, id: id, nonce: nonce, rep: rep, t: t},
				success: function(response) {
					jQuery("[post_id="+id+"]").siblings(".after_report").removeAttr('style');
					loading = false;
					if(response.type=="success"){
						jQuery("[post_id="+id+"]").siblings(".after_report").html('Report sent');
					} else if(response.type=="exists"){
						jQuery("[post_id="+id+"]").siblings(".after_report").html('You already reported this');
					}
				}
			});
		}
	});
	jQuery(".save_btn").click( function() {
		if (!loading){
			nonce = jQuery(this).attr("nonce");
			id = jQuery(this).attr("post_id");
			action = jQuery(this).attr("action");
			t = jQuery(this).attr("type");
			jQuery(this).html('Saving...')
			loading = true;
			jQuery.ajax({
				type : "post",
				dataType : "json",
				url : ajaxurl,
				data : {action: action, id: id, nonce: nonce, t:t},
				success: function(response) {
					if(response.type=="success"){
						jQuery(".save_btn[type="+t+"][post_id="+id+"]").html('Post saved');
					} else {
						jQuery(".save_btn[type="+t+"][post_id="+id+"]").html('You already saved this.');
					}
					loading = false;
				}
			});
		}
	});
	jQuery(".admin_func").click(function(){
		if (!loading){
			nonce = jQuery(this).attr("nonce");
			id = jQuery(this).attr("post_id");
			uid = jQuery(this).attr("data-user");
			action = jQuery(this).attr("action");
			t = jQuery(this).attr("type");
			msg = jQuery(this).siblings('.usernoteinput').val();
			jQuery(this).after('<img src="'+plugdir+'/feddit/images/loading.gif"  id="posts_loading" />');
			loading = true;
			jQuery.ajax({
				type : "post",
				dataType : "json",
				url : ajaxurl,
				data : {action: action, id: id, nonce: nonce, t:t, user_id:uid, msg:msg},
				success: function(response) {
					if(response.type=="success"){
						if(response.color){
							jQuery("#fpost_"+id).css("background-color",response.color);
						}
						jQuery("#posts_loading").attr('src', plugdir+'/feddit/images/check.png').delay( 800 ).fadeOut( 400 );
						jQuery("#posts_loading").removeAttr('id');
						loading = false;
					} else {
						jQuery("#posts_loading").attr('src', plugdir+'/feddit/images/x.png');
						alert(response.type);
					}
				}
			});
		}
	});
	jQuery("#user_tabs span").click( function() {
		jQuery(this).siblings().removeClass("selected");
		jQuery(this).addClass("selected");
		jQuery('.user_wrap.selected').toggle();
		jQuery('.user_wrap.selected').removeClass("selected");
		jQuery('#user_'+jQuery(this).attr("tab")).toggle();
		jQuery('#user_'+jQuery(this).attr("tab")).addClass("selected");
		
	});
});
